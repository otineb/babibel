var searchData=
[
  ['ok',['OK',['../_return_value_8hpp.html#aa92f3569028c07fdd794de95cc205850ae0aa021e21dddbd6d8cecec71e9cf564',1,'ReturnValue.hpp']]],
  ['ongoing',['onGoing',['../structbabel_server_1_1_call.html#af85400f2876fd8432bab829775df01cb',1,'babelServer::Call']]],
  ['openfile',['openFile',['../class_file_manager.html#a677e7d599a2dfef140278f8e9b272f5a',1,'FileManager']]],
  ['openstream',['openStream',['../class_stream.html#ae6ace1f1386e2b638fdc48303d3d90dc',1,'Stream']]],
  ['operator_3d',['operator=',['../class_error_audio.html#af4004ed9ae0f1feea1893742f4565bca',1,'ErrorAudio::operator=()'],['../class_error_opus.html#a78d97f747ce50ab54bd5655bf607b36b',1,'ErrorOpus::operator=()']]],
  ['opusmanager',['OpusManager',['../class_opus_manager.html',1,'OpusManager'],['../class_opus_manager.html#a9c2e8f22c0ee0bbc0e0dc25215b0cfda',1,'OpusManager::OpusManager()']]],
  ['opusmanager_2ecpp',['OpusManager.cpp',['../_opus_manager_8cpp.html',1,'']]],
  ['opusmanager_2ehpp',['OpusManager.hpp',['../_opus_manager_8hpp.html',1,'']]]
];
