var searchData=
[
  ['unfriend',['UnFriend',['../class_event.html#a0d1f4e7a8066662e8ecf4a72a2f7e3bba0424c123bc90dad8228dcf4029c06e27',1,'Event']]],
  ['unknown',['UNKNOWN',['../client_2_action_type_8hpp.html#a3a5a4bab89ac6605d5282b496888fb6da696b031073e74bf2cb98e5ef201d4aa3',1,'UNKNOWN():&#160;ActionType.hpp'],['../namespacebabel_server.html#afd827cb774b06bd9a79e38f6c7e0448ca696b031073e74bf2cb98e5ef201d4aa3',1,'babelServer::UNKNOWN()']]],
  ['unknown_5ferror',['UNKNOWN_ERROR',['../_return_value_8hpp.html#aa92f3569028c07fdd794de95cc205850a9a10f345b0f755d461a8673a5e9ef6b4',1,'ReturnValue.hpp']]],
  ['update_5fcontact_5flist',['UPDATE_CONTACT_LIST',['../namespacebabel_server.html#afd827cb774b06bd9a79e38f6c7e0448caa3137d56d498c2c7b714462eb5198f04',1,'babelServer']]],
  ['updatecontactlist',['UpdateContactList',['../class_event.html#a0d1f4e7a8066662e8ecf4a72a2f7e3bba5946429911091d2021564200b7bf9ac1',1,'Event::UpdateContactList()'],['../client_2_action_type_8hpp.html#a3a5a4bab89ac6605d5282b496888fb6da15831e175bbb88e9e3e8b6135b8f0355',1,'UPDATECONTACTLIST():&#160;ActionType.hpp']]],
  ['username_5fexists',['USERNAME_EXISTS',['../_return_value_8hpp.html#aa92f3569028c07fdd794de95cc205850a85f315d655d4c8cd913ebf89845fd100',1,'ReturnValue.hpp']]]
];
