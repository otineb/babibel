#include "include/client/Event/EventManager.hpp"
#include "include/client/Client.hpp"

	namespace Event
	{
		EventManager			*EventManager::_instance = NULL;
		EventManager			*EventManager::GetSingleton() { return (_instance); }

		EventManager::EventManager()
		{
			if (EventManager::_instance == NULL)
				EventManager::_instance = this;

		}

		EventManager::~EventManager()
		{
			if (EventManager::_instance == this)
				EventManager::_instance = NULL;
		}

		std::vector<Event>		EventManager::GetEventList()
		{
			this->_eventListMtx.lock();

				std::vector<Event>	list(this->_eventList);
				this->_eventList.clear();
			
			this->_eventListMtx.unlock();

			return (list);
		}

		bool					EventManager::IsEmpty()
		{
			bool				ret;
			this->_eventListMtx.lock();

			ret = (this->_eventList.size() == 0);

			this->_eventListMtx.unlock();
			return (ret);
		}

		void					EventManager::AddEvent(EventType type)
		{
			Event				newEvent(type);

			if (!Client::GetSingleton()->TreatEvent(newEvent))
			{
				this->_eventListMtx.lock();

					this->_eventList.push_back(newEvent);

				this->_eventListMtx.unlock();
			}
		}

		void					EventManager::AddEvent(EventType type, std::vector<std::string> const &data)
		{
			Event				newEvent(type, data);
	
			if (!Client::GetSingleton()->TreatEvent(newEvent))
			{
				this->_eventListMtx.lock();

					this->_eventList.push_back(newEvent);

				this->_eventListMtx.unlock();
			}
		}

		void					EventManager::Singleton_AddEvent(EventType type)
		{
			if (EventManager::GetSingleton())
				EventManager::GetSingleton()->AddEvent(type);
		}

		void					EventManager::Singleton_AddEvent(EventType type, std::vector<std::string> const &data)
		{
			if (EventManager::GetSingleton())
				EventManager::GetSingleton()->AddEvent(type, data);
		}
	}