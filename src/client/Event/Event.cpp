#include "include/client/Event/Event.hpp"

	namespace Event
	{
		Event::Event(EventType type) :
			_type(type)
		{
		}

		Event::Event(EventType type, std::vector<std::string> const &data) :
			_type(type),
			_data(data)
		{
		}

		Event::~Event()
		{

		}

		std::vector<std::string> const &	Event::Data() const { return (this->_data); }
		EventType const &					Event::Type() const { return (this->_type); }
	}