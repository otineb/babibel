//
// TcpSocket.cpp for TcpSocket.cpp in /home/daze/rendu/c++/babibel/QT_network_test
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Thu Sep 28 16:15:30 2017 DaZe
// Last update Mon Oct  2 19:07:49 2017 DaZe
//

#include "include/client/Utils.hpp"
#include "include/client/TcpSocket.hpp"
#include "include/client/Event/EventManager.hpp"

TcpSocket::TcpSocket()
{
    _socket = new QTcpSocket;
    _bufferSize = 0;
    _port = 1024;
}

TcpSocket::TcpSocket(std::string ip) : _ip(ip), _port(1024)
{
  _socket = new QTcpSocket;
}

void			TcpSocket::doConnect()
{
  _socket = new QTcpSocket(this);

  connect(_socket, SIGNAL(connected()),this, SLOT(connected()));
  connect(_socket, SIGNAL(disconnected()),this, SLOT(disconnected()));
  connect(_socket, SIGNAL(bytesWritten(qint64)),this, SLOT(bytesWritten(qint64)));
  connect(_socket, SIGNAL(readyRead()),this, SLOT(readyRead()));

  _socket->connectToHost(QString::fromStdString(_ip), quint16(_port));
  
  if(!_socket->waitForConnected(5000))
    {
      throw std::runtime_error("Error: Can't connect to server");
    }
}

void TcpSocket::connected()
{
  qInfo() << "connected...";
}

void TcpSocket::disconnected()
{
  qInfo()<< "disconnected...";
}

void TcpSocket::bytesWritten(qint64)
{}

void TcpSocket::readyRead()
{
    ActionType code;
    if (_bufferSize == 0)
    {
        _buffer = _socket->read(4);
        _bufferSize = Utils::QByteArrayToInt(_buffer);
    }
    if (_socket->bytesAvailable() >= _bufferSize)
    {
        _buffer = _socket->read(2);
        code = Utils::QByteArrayToUS(_buffer);
        _buffer = _socket->read(_bufferSize - 2);
        if (_socket->bytesAvailable() == 0)
            dataToEvent(_bufferSize - 2, code, _buffer);
        _bufferSize = 0;
    }
  if (_socket->bytesAvailable() > 0)
    readyRead();
}

void			TcpSocket::write(const struct Packet &toWrite)
{
    _socket->write(QByteArray(reinterpret_cast<const char *>(&toWrite),
                              sizeof(toWrite.size) + sizeof(toWrite.type)));
    _socket->write(toWrite.data, toWrite.size - 2);
}

QTcpSocket		*TcpSocket::getSocket() const
{
    return (_socket);
}

QByteArray const &    TcpSocket::getBuffer() const
{
    return (_buffer);
}

void                    TcpSocket::setBuffer(QByteArray const &buffer)
{
    _buffer = buffer;
}

void			TcpSocket::setIp(std::string const &ip)
{
  _ip = ip;
}

void			TcpSocket::setPort(int port)
{
  _port = port;
}

const std::string &	TcpSocket::getIp() const
{
  return (_ip);
}

int			TcpSocket::getPort() const
{
   return (_port);
}

bool        TcpSocket::TreatEvent(Event::Event const &)
{
   return true;
}

void        TcpSocket::dataToEvent(int size, ActionType codeAction, QByteArray const &buffer)
{
    auto it = _handler.getMap().find(codeAction);
    if (it != _handler.getMap().end()) {
        auto ev = it->second(buffer);
        Event::EventManager::Singleton_AddEvent(ev, Utils::QByteArrayToStringVec(size, buffer));
    }
}
