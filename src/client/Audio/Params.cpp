//
// Created by pashervz on 29/09/17.
//

#include <iostream>
#include "client/Audio/Params.hpp"
#include "client/Audio/ErrorAudio.hpp"

Params::Params(DeviceType device)
{
    if (Pa_Initialize() != paNoError)
        throw ErrorAudio("PortAudio init error");
    if (device == AUDIO_INPUT)
        InitInputParamsDefault();
    else
        InitOutputParamsDefault();
}

void            Params::InitInputParamsDefault()
{
    _params.device = Pa_GetDefaultInputDevice();
    if (_params.device == paNoDevice)
        throw ErrorAudio("No input device detected");
    _params.channelCount = MONO;
    _params.sampleFormat = paInt16;
    _params.suggestedLatency = Pa_GetDeviceInfo(_params.device)->defaultLowInputLatency;
    _params.hostApiSpecificStreamInfo = nullptr;
}

void            Params::InitOutputParamsDefault()
{
    _params.device = Pa_GetDefaultOutputDevice();
    if (_params.device == paNoDevice)
        throw ErrorAudio("No output device detected");
    _params.channelCount = MONO;
    _params.sampleFormat = paInt16;
    _params.suggestedLatency = Pa_GetDeviceInfo(_params.device)->defaultLowOutputLatency;
    _params.hostApiSpecificStreamInfo = nullptr;
}
