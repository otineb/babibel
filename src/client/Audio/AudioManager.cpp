//
// Created by pashervz on 28/09/17.
//

#include <portaudio.h>
#include <iostream>
#include "client/Audio/AudioManager.hpp"
#include "client/Audio/ErrorAudio.hpp"

AudioManager::AudioManager() {}

AudioManager::~AudioManager()
{
    this->Terminate();
}

void                    AudioManager::init()
{
    this->_stream.openStream();
}

void                    AudioManager::launch()
{
    this->_stream.startStream();
}

void                    AudioManager::stop()
{
    this->_stream.closeStream();
}

std::pair<int, unsigned char *> AudioManager::getRecDataToSend()
{
    auto p = this->_stream.sendRecData();
    return (std::make_pair(p.first, p.second.data()));
}

void                    AudioManager::pushData(unsigned char *data)
{
    int *size = reinterpret_cast<int *>(data);
    unsigned char *encData = data + sizeof(int);
    this->_stream.putPlayData(*size, encData);
}

void                    AudioManager::Terminate()
{
    if (Pa_Terminate() != paNoError)
        throw ErrorAudio("PortAudio term error");
}
