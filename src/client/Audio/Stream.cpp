//
// Created by pashervz on 29/09/17.
//

#include <iostream>
#include <cstring>
#include <memory>
#include <include/client/Audio/ErrorAudio.hpp>
#include "client/Audio/Stream.hpp"

const int               Stream::RATE = 48000;
const unsigned long     Stream::OPTIFRAMES = 480;

Stream::Stream() : _outputParams(Params(AUDIO_OUTPUT)),
		   _inputParams(Params(AUDIO_INPUT))
{
    _compressor.createEncoder(RATE, MONO);
    _compressor.createDecoder(RATE, MONO);
}

int                                 Stream::callBack(const void *inputBuffer,void *outputBuffer,
                                                     unsigned long frameSize, const PaStreamCallbackTimeInfo*,
                                                     PaStreamCallbackFlags, void *data)
{
    auto                            *recBuffer = static_cast<const short *>(inputBuffer);
    auto                            *playBuffer = static_cast<short *>(outputBuffer);
    auto                            *stream = static_cast<Stream *>(data);
    std::vector<short>              toPush;

    if (recBuffer != nullptr) {
        for (long unsigned int i = 0; i < frameSize; ++i) {
            toPush.push_back(recBuffer[i]);
        }
    }
    try
    {
        if (!toPush.empty()) {
            stream->_compressor.setRecData(toPush);
            stream->_compressor.doEncode(frameSize);
            stream->_recQueue.push(std::make_pair(stream->_compressor.getNbEncodedBytes(),
                                                  stream->_compressor.getToSendData()));
        }
        if (stream->_playQueue.empty())
            return (paContinue);
	stream->_compressor.setReceivedData(stream->_playQueue.front().second);
        stream->_compressor.doDecode(frameSize, stream->_playQueue.front().first);
        for (unsigned long i = 0; i < frameSize; ++i) {
            playBuffer[i] = stream->_compressor.getDecodData()[i];
        }
        stream->_playQueue.pop();
    }
    catch (std::exception const & msg) {
        std::cout << msg.what() << std::endl;
        stream->_playQueue.pop();
        return (paContinue);
    }
    return paContinue;
}

void                    Stream::openStream()
{
    if (Pa_OpenStream(&_stream, &_inputParams._params, &_outputParams._params,
                  RATE, OPTIFRAMES, paClipOff, Stream::callBack, this) != paNoError)
        throw ErrorAudio("Opening Stream failed");
}

void                   Stream::startStream()
{
    if (Pa_StartStream(_stream) != paNoError)
        throw ErrorAudio("Starting Stream failed");
}

void                    Stream::closeStream()
{
    if (Pa_CloseStream(_stream) != paNoError)
        throw ErrorAudio("Closing stream failed");
}

std::pair<int, std::vector<unsigned char>>      Stream::sendRecData()
{
    std::pair<int, std::vector<unsigned char>>  toSend;

    if (this->_recQueue.empty())
      return (toSend);
    auto nb = this->_recQueue.front().first;
    auto enc = this->_recQueue.front().second;
    this->_recQueue.pop();
    toSend.first = nb;
    toSend.second = enc;
    return (toSend);
}

void                            Stream::putPlayData(int size, unsigned char *data)
{

  std::pair<int, std::vector<unsigned char>>  toPush;

    if (data == nullptr)
        return ;
    toPush.first = size;
    for (long unsigned int i = 0; i < OPTIFRAMES; ++i)
      {
    	toPush.second.push_back(data[i]);
      }
    _playQueue.push(toPush);
}
