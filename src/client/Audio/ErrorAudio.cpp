//
// Created by pashervz on 28/09/17.
//

#include "client/Audio/ErrorAudio.hpp"

ErrorAudio::ErrorAudio(std::string const& errorMessage) throw() : _errorMessage("Audio Error: " + errorMessage) {}

ErrorAudio::~ErrorAudio() throw() {}

ErrorAudio::ErrorAudio(ErrorAudio const & other) :  _errorMessage(other._errorMessage) {}

ErrorAudio&                  ErrorAudio::operator=(ErrorAudio const & other)
{
    if (this != &other)
    {
        _errorMessage = other._errorMessage;
    }
    return (*this);
}

const char              *ErrorAudio::what() const throw()
{
    return (_errorMessage.c_str());
}
