//
// Created by pashervz on 03/10/17.
//

#include "client/Audio/ErrorOpus.hpp"

ErrorOpus::ErrorOpus(std::string const& errorMessage) throw() : _errorMessage("Opus Error: " + errorMessage) {}

ErrorOpus::~ErrorOpus() throw() {}

ErrorOpus::ErrorOpus(ErrorOpus const & other) :  _errorMessage(other._errorMessage) {}

ErrorOpus&                  ErrorOpus::operator=(ErrorOpus const & other)
{
    if (this != &other)
    {
        _errorMessage = other._errorMessage;
    }
    return (*this);
}

const char              *ErrorOpus::what() const throw()
{
    return (_errorMessage.c_str());
}
