//
// Created by pashervz on 03/10/17.
//

#include <vector>
#include <iostream>
#include "client/Audio/OpusManager.hpp"
#include "client/Audio/ErrorOpus.hpp"

OpusManager::~OpusManager()
{
    opus_encoder_destroy(_encoder);
    opus_decoder_destroy(_decoder);
}

void            OpusManager::createEncoder(int rate, int nbChannels)
{
    int         error;

    _encoder = opus_encoder_create(rate, nbChannels, OPUS_APPLICATION_VOIP, &error);
    if (error != OPUS_OK)
        throw ErrorOpus("encoder creation failed");
}

void            OpusManager::createDecoder(int rate, int nbChannels)
{
    int         error;

    _decoder = opus_decoder_create(rate, nbChannels, &error);
    if (error != OPUS_OK)
        throw ErrorOpus("decoder creation failed");
}

void            OpusManager::doEncode(int frameSize)
{
    _toSendData.resize(frameSize * 2);
    if ((_nbEncodedBytes = opus_encode(_encoder, _recData.data(),
			     frameSize, _toSendData.data(), _toSendData.size())) < 0)
      throw ErrorOpus("encoding failed");
}

void            OpusManager::doDecode(int frameSize, int size)
{
    opus_int32  check;

    _decodData.resize(frameSize * 2);
    if ((check = opus_decode(_decoder, _receivedData.data(), size,
                             _decodData.data(), frameSize, 0) < 0))
        throw ErrorOpus("decoding failed");
}

void            OpusManager::setRecData(std::vector<short> const & data)
{
  _recData = data;
}

void		OpusManager::setToSendData(std::vector<unsigned char> const & data)
{
  _toSendData = data;
}

void		OpusManager::setReceivedData(std::vector<unsigned char> const & data)
{
  _receivedData = data;
}
