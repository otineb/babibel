//
// UdpSocket.cpp for UdpSocket.cpp in /home/daze/rendu/c++/babibel/QT_network_test
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Thu Sep 28 16:15:30 2017 DaZe
// Last update Sat Oct  7 22:26:29 2017 daze
//

#include "include/client/UdpSocket.hpp"
#include <iostream>

UdpSocket::UdpSocket(std::string const &ip) :
  _portRead(1024),
  _portWrite(1024)
{
  QString	_ip = QString::fromStdString(ip);


  _destAddress.setAddress(_ip);
  _audioMgr.init();
  run();
}

UdpSocket::~UdpSocket()
{
  _audioMgr.stop();
}

void			UdpSocket::run()
{
  _audioMgr.launch();

  this->_socket.bind(QHostAddress::Any, _portRead);
}

void			UdpSocket::sendDatagram()
{
  QByteArray	data;

  auto p = _audioMgr.getRecDataToSend();

  data.append(reinterpret_cast<char *>(&p.first), sizeof(p.first));
  data.append(reinterpret_cast<char *>(p.second), p.first);
  if (data.size() == 4)
    return ;
  this->_socket.writeDatagram(data, _destAddress, _portWrite);
}

void			UdpSocket::readPendingDatagrams()
{
  QByteArray	datagram;


  datagram.resize(this->_socket.pendingDatagramSize());
  this->_socket.readDatagram(datagram.data(), datagram.size());
  if (datagram.size() == 0)
    return ;
  _audioMgr.pushData(reinterpret_cast<unsigned char *>(datagram.data()));
}

void			UdpSocket::setReceiverAddress(std::string const &str)
{
  QString	ip;

  ip = QString::fromStdString(str);
  _destAddress.setAddress(ip);  
}

QHostAddress		UdpSocket::getReceiverAddress()
{
  return (this->_destAddress);
}
