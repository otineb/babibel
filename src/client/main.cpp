#include <QCoreApplication>
#include <iostream>

#include "include/client/Client.hpp"

int main(int ac, char **av)
{
	try
	{
		if (ac != 2)
			throw std::runtime_error("Usage : [SERVER IP]");
		Client      client(ac, av);

		client.getNetMgr()->getTcpSocket()->setIp(std::string(av[1]));

		client.getNetMgr()->getTcpSocket()->doConnect();

		return (client.Execute());
	}
    catch (std::exception const &e)
	{
		std::cout << e.what() << std::endl;
		return (1);
    }
}
