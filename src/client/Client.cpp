//
// Client.cpp for Client.cpp in /home/daze/rendu/c++/babibel
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Fri Sep 29 15:49:46 2017 DaZe
// Last update Sat Oct  7 14:33:21 2017 daze
//

#include <condition_variable>

#include "include/client/Client.hpp"

Client		*Client::_instance = NULL;
Client		*Client::GetSingleton() { return (Client::_instance); }

Client::Client(int argc, char **argv) :
  _graphMgr(argc, argv),
  _eventMgr(),
  _state(State::Start),
  av(argv)
{
  _netMgr = new NetworkMgr();
  if (_instance == NULL)
    _instance = this;

  _eventUsers.push_back(&_graphMgr);
}

Client::~Client()
{
  if (Client::_instance == this)
    Client::_instance = NULL;
}

int        Client::thread_Network()
{
  while (_state != State::End)
    {
      if (_state == State::CallBegin)
      {
          _netMgr->connectUdp(_ipOtherClient);
          _state = State::Calling;
      }
      if (_state == State::Calling)
        {
            this->_netMgr->_udpSocket->sendDatagram();
            this->_netMgr->_udpSocket->readPendingDatagrams();
	    }

        if (_state == State::EndCall)
        {
            this->_netMgr->disconnectUdp();
            _state = State::Default;
        }
    }
    return (0);
}

int			Client::Execute()
{

    _state = State::Default;

    std::thread		thread_Event(&Client::thread_Event, this);
    std::thread		thread_Network(&Client::thread_Network, this);

    int 			errVal;

    errVal = this->thread_Graphical();

    _state = State::End;

    thread_Event.join();
    thread_Network.join();
    return (errVal);
}


int			Client::thread_Graphical()
{
  return (this->_graphMgr.Execute());
}

int			Client::thread_Event()
{
  //std::condition_variable			cond_variable;
  //std::unique_lock<std::mutex>	mtx(_mtx);

  //cond_variable.wait(
  //while (this->_state != State::End)
  //{
  //this->_eventMgr.IsEmpty();
  //}
  return (0);
}

	bool		Client::TreatEvent(Event::Event const &event)
	{
        /*
		switch (event.Type())
        {
			case(Event::EventType::Connect):
			case(Event::EventType::CreateAccount):

				SetClientName(event.Data()[0]);

				break;
             */

                /*
			case(Event::EventType::Calling):
			case(Event::EventType::ReceiveCall):
				SetCallerName(event.Data()[0]);
				break;


			default:
				break;

		}*/

		if (event.Type() == Event::EventType::UpdateContactList)
			_friendList = event.Data();
		if (event.Type() == Event::EventType::ReceiveCall || event.Type() == Event::EventType::Calling)
			_callerName = event.Data()[0];

		for (IEventUser * eventUser : _eventUsers)
		{
			eventUser->TreatEvent(event);
		}
        commandHandler(event.Type(), event.Data());

        if (event.Type() == Event::EventType::ReceiveCall)
        {
            _ipOtherClient = event.Data()[1];
        }

		if (event.Type() == Event::EventType::CallBegin)
		{
            if (event.Data().size() > 1)
                _ipOtherClient = event.Data()[1];
            _state = State::CallBegin;
		}
		if (event.Type() == Event::EventType::CallEnd || event.Type() == Event::EventType::HangUp)
		{
			_state = State::EndCall;
		}
		
		return (true);
	}

	std::string	const &					Client::GetClientName() const { return (_clientName); }
	void								Client::SetClientName(std::string const &newString) { _clientName = newString; }

	std::string	const &					Client::GetCallerName() const { return (_callerName); }
	void								Client::SetCallerName(std::string const &newString) { _callerName = newString; }

	std::vector<std::string> const &	Client::GetClientFriendList() const { return (_friendList); }

void							Client::SetClientFriendList(std::vector<std::string> const &newList) { _friendList = std::vector<std::string>(newList); }

    void Client::commandHandler(Event::EventType ev, std::vector<std::string> const& args)
    {
        struct Packet packet;
      /*  for (auto const &it : _actions) {
            if (it.first == type)
                it.second(s1, s2, packet);
        }*/

        auto it = _actions.find((ActionType)ev);
        if (it != _actions.end())
        {
            it->second(args, packet);
        }
    }

    void Client::registration(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 2)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        auto& s2 = args[1];
        std::string tmp;
        packet.size = s1.size() + s2.size() + 3;
        packet.type = 100;
        tmp = s1 + '\0' + s2;
        packet.data = tmp.c_str();
        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::deletion(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 110;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::connection(std::vector<std::string> const& args, struct Packet &packet) {
      if (args.size() < 2)
	throw std::runtime_error("Wrong number of args");
      auto& s1 = args[0];
      auto& s2 = args[1];
      std::string tmp;
      packet.size = s1.size() + s2.size() + 3;
      packet.type = 120;
      tmp = s1 + '\0' + s2;
      packet.data = tmp.c_str();
      _netMgr->getTcpSocket()->write(packet);
    }

    void Client::disconnection(std::vector<std::string> const&, struct Packet &packet) {
        packet.size = 3;
        packet.type = 130;
        packet.data = "";

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::getContactList(std::vector<std::string> const&, struct Packet &packet) {
        packet.size = 3;
        packet.type = 140;
        packet.data = "";

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::getInvitationList(std::vector<std::string> const&, struct Packet &packet) {
        packet.size = 3;
        packet.type = 150;
        packet.data = "";

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::getContact(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 160;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

/* ATTENTION AU + 3 ou +2 COMME JUSTE EN DESSOUS (THOMAS/BENOIT)*/

    void Client::addContact(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 170;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::acceptContact(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 180;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::declineContact(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 190;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::delContact(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 180;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::changePasswd(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 2)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        auto& s2 = args[1];
        packet.size = s1.size() + s2.size() + 7;
        packet.type = 210;
        packet.data = std::string(s1 + " " + s2).c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::startSession(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 220;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::stopSession(std::vector<std::string> const&, struct Packet &packet) {
        packet.size = 3;
        packet.type = 230;
        packet.data = "";

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::acceptSession(std::vector<std::string> const&, struct Packet &packet) {
        packet.size = 3;
        packet.type = 240;
        packet.data = "";

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::declineSession(std::vector<std::string> const&, struct Packet &packet) {
        packet.size = 3;
        packet.type = 250;
        packet.data = "";

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::addToSession(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 260;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    void Client::kickFromSession(std::vector<std::string> const& args, struct Packet &packet) {
        if (args.size() < 1)
            throw std::runtime_error("Wrong number of args");
        auto& s1 = args[0];
        packet.size = s1.size() + 3;
        packet.type = 270;
        packet.data = s1.c_str();

        _netMgr->getTcpSocket()->write(packet);
    }

    NetworkMgr *Client::getNetMgr() const {
        return (_netMgr);
    }
