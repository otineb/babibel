# include "include/client/Graphical/GraphicalManager.hpp"

# include "include/client/Graphical/QWindows/InscriptionWindows.hpp"
# include "include/client/Graphical/QWindows/DefaultMenuWindows.hpp"
# include "include/client/Graphical/QWindows/CallingWindows.hpp"
# include "include/client/Graphical/QWindows/InCallWindows.hpp"
# include "include/client/Graphical/QWindows/AddAFriendWindows.hpp"

namespace Graphical
	{
		GraphicalManager		*GraphicalManager::_instance = NULL;
		GraphicalManager		*GraphicalManager::GetSingleton() { return (_instance); }

		GraphicalManager::GraphicalManager(int argc, char **argv) :
			_App(argc, argv),
			_MainWindows()
		{
			if (GraphicalManager::_instance == NULL)
				GraphicalManager::_instance = this;

//			Q_INIT_RESOURCE(ressources);
			
			InitWindows();

			_MainWindows.show();
			ShowSubView(WindowType::Inscription);
			
			/*
			if (QResource::registerResource("ressources.cc"))
				std::cout << "Success" << std::endl;
			else
				std::cout << "Failed to load the lib" << std::endl;
			*/

			QFile file("ressource/stylesheet.qss");

			if (file.open(QFile::OpenModeFlag::ReadOnly))
			{
				QString str(file.readAll());

				_App.setStyleSheet(str);
			}
		}

		GraphicalManager::~GraphicalManager()
		{
			if (GraphicalManager::_instance == this)
				GraphicalManager::_instance = NULL;
		}

		void					GraphicalManager::InitWindows()
		{
			std::vector<IWindows*>	windows;

			windows.push_back(new InscriptionWindows(this->_MainWindows));
			windows.push_back(new DefaultMenuWindows(this->_MainWindows));
			windows.push_back(new CallingWindows(this->_MainWindows));
			windows.push_back(new InCallWindows(this->_MainWindows));
			windows.push_back(new AddAFriendWindows(this->_MainWindows));

			for (IWindows *window : windows)
			{
				std::shared_ptr<IWindows>	windowsPtr(window);
				
				windowsPtr->HideWindows();
				_windows.push_back(windowsPtr);
			}
		}

		int						GraphicalManager::Execute()
		{
			return (this->_App.exec());
		}

		bool						GraphicalManager::TreatEvent(Event::Event const &event)
		{
			switch (event.Type())
			{
				case (Event::EventType::ConnectionAcc):
					HideSubView(WindowType::Inscription);
					ShowSubView(WindowType::DefaultMenu);
					break;

				case (Event::EventType::ReceiveCall):
					ShowSubView(WindowType::Calling);
					break;

				case (Event::EventType::CallAccepted):
					HideSubView(WindowType::Calling);
					break;
				
				case (Event::EventType::CallRefused):
					HideSubView(WindowType::Calling);
					break;

				case (Event::EventType::CallBegin):
					ShowSubView(WindowType::InCall);
					break;

				case (Event::EventType::CallEnd):
				case (Event::EventType::HangUp):
					HideSubView(WindowType::InCall);
					break;

				case (Event::EventType::AddAFriend):
					ShowSubView(WindowType::AddFriend);
					break;

				case (Event::EventType::FriendAdded):
				case (Event::EventType::CancelAddFri):
					HideSubView(WindowType::AddFriend);
					break;

				default:
					break;
			}

			for (std::shared_ptr<IWindows> window : _windows)
			{
				if (window->Showed())
					window->TreatEvent(event);
			}

			return (false);
		}

		void						GraphicalManager::ShowSubView(WindowType const & type)
		{
			std::shared_ptr<IWindows> window = GetSubView(type);

			if (window == NULL)
				return;

			window->ShowWindows();
		}

		void						GraphicalManager::HideSubView(WindowType const & type)
		{
			std::shared_ptr<IWindows> window = GetSubView(type);

			if (window == NULL)
				return;

			window->HideWindows();
		}

		std::shared_ptr<IWindows>	GraphicalManager::GetSubView(WindowType const & type) const
		{
			for (std::shared_ptr<IWindows> window : _windows)
			{
				if (window->Type() == type)
					return (window);
			}

			return (NULL);
		}
	}