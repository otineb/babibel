#include "include/client/Graphical/ClientApplication.hpp"
#include <QFile>
#include <iostream>

	namespace Graphical
	{
		ClientApplication::ClientApplication(int argc, char **argv)
			: QApplication(argc, argv)
		{
		  QFile file("../ressource/stylesheet.qss");

		  if (file.open(QFile::OpenModeFlag::ReadOnly))
		    {
		      QString str(file.readAll());

		      setStyleSheet(str);
		    }
		}

		ClientApplication::~ClientApplication()
		{

		}

		bool ClientApplication::event(QEvent *event)
		{
			return (QApplication::event(event));
		}
	}