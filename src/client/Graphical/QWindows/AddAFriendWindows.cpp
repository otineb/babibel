#include "include/client/Client.hpp"

#include "include/client/Graphical/QWindows/AddAFriendWindows.hpp"
#include "include/client/Event/EventManager.hpp"

	namespace Graphical
	{
		AddAFriendWindows::AddAFriendWindows(QWidget & Windows)
			: QWidget(&Windows)
		{
			this->setupUi();
			this->retranslateUi();
			this->setupConnectSlot();
		}

		AddAFriendWindows::~AddAFriendWindows()
		{
		}

		void									AddAFriendWindows::setupConnectSlot()
		{
			connect(Button_Add, SIGNAL(clicked()), this, SLOT(Pressed_Add()));
			connect(Button_Cancel, SIGNAL(clicked()), this, SLOT(Pressed_Cancel()));
		}

		void									AddAFriendWindows::setupUi()
		{
			if (this->objectName().isEmpty())
				this->setObjectName(QStringLiteral("Windows"));
			this->resize(480, 139);
			this->move(0, 750);
			this->setMinimumSize(QSize(480, 100));
			this->setMaximumSize(QSize(480, 139));
			this->setStyleSheet(QStringLiteral(""));
			
			verticalLayout = new QVBoxLayout(this);
			verticalLayout->setSpacing(0);
			verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
			verticalLayout->setContentsMargins(40, 0, 40, 0);
			Frame_1 = new QFrame(this);
			Frame_1->setObjectName(QStringLiteral("Frame_1"));
			Frame_1->setMinimumSize(QSize(400, 75));
			Frame_1->setMaximumSize(QSize(400, 75));
			Frame_1->setFrameShape(QFrame::StyledPanel);
			Frame_1->setFrameShadow(QFrame::Raised);
			horizontalLayout = new QHBoxLayout(Frame_1);
			horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
			Label_Name = new QLabel(Frame_1);
			Label_Name->setObjectName(QStringLiteral("Label_Name"));

			horizontalLayout->addWidget(Label_Name);

			Edit_Name = new QLineEdit(Frame_1);
			Edit_Name->setObjectName(QStringLiteral("Edit_Name"));
			Edit_Name->setStyleSheet(QStringLiteral(""));

			horizontalLayout->addWidget(Edit_Name);

			verticalLayout->addWidget(Frame_1);

			Label_Error = new QLabel(this);
			Label_Error->setObjectName(QStringLiteral("Label_Error"));
			Label_Error->setMinimumSize(QSize(400, 25));
			Label_Error->setMaximumSize(QSize(400, 25));
			Label_Error->setStyleSheet(QStringLiteral("color: rgb(220, 20, 20);"));
			Label_Error->setAlignment(Qt::AlignCenter);

			verticalLayout->addWidget(Label_Error);

			Frame_2 = new QFrame(this);
			Frame_2->setObjectName(QStringLiteral("Frame_2"));
			Frame_2->setFrameShape(QFrame::StyledPanel);
			Frame_2->setFrameShadow(QFrame::Raised);
			horizontalLayout_2 = new QHBoxLayout(Frame_2);
			horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
			Button_Cancel = new QPushButton(Frame_2);
			Button_Cancel->setObjectName(QStringLiteral("Button_Cancel"));
			Button_Cancel->setMinimumSize(QSize(100, 25));
			Button_Cancel->setMaximumSize(QSize(100, 25));

			horizontalLayout_2->addWidget(Button_Cancel);

			Button_Add = new QPushButton(Frame_2);
			Button_Add->setObjectName(QStringLiteral("Button_Add"));
			Button_Add->setMinimumSize(QSize(100, 25));
			Button_Add->setMaximumSize(QSize(100, 25));

			horizontalLayout_2->addWidget(Button_Add);


			verticalLayout->addWidget(Frame_2);
		}

		void									AddAFriendWindows::retranslateUi()
		{
			this->setWindowTitle(QApplication::translate("Windows", "Windows", Q_NULLPTR));
			Label_Name->setText(QApplication::translate("Windows", "Friend name:", Q_NULLPTR));
			Button_Add->setText(QApplication::translate("Windows", "Add", Q_NULLPTR));
			Button_Cancel->setText(QApplication::translate("Windows", "Cancel", Q_NULLPTR));

			Label_Error->setText(QString());
		}

		void									AddAFriendWindows::updateValue()
		{
			Label_Error->setText(QString());
		}

		WindowType								AddAFriendWindows::Type() const { return (WindowType::AddFriend); }

		void									AddAFriendWindows::ShowWindows()
		{
			this->updateValue();

			this->show();
			_showed = true;
		}

		void									AddAFriendWindows::HideWindows()
		{
			this->hide();
			_showed = false;
		}

		void									AddAFriendWindows::Pressed_Add()
		{
			std::vector<std::string>			name;
			name.push_back(Edit_Name->text().toStdString());

			Button_Add->setDisabled(true);

			Event::EventManager::Singleton_AddEvent(Event::EventType::TryAddFriend, name);
		}

		void									AddAFriendWindows::Pressed_Cancel()
		{
			Event::EventManager::Singleton_AddEvent(Event::EventType::CancelAddFri);
		}

		bool									AddAFriendWindows::TreatEvent(Event::Event const &event)
		{
			switch (event.Type())
			{

			case (Event::EventType::FriendAddErr):
				Label_Error->setText(QString::fromStdString("Error: Failed to add a friend: " + event.Data()[0]));
				Button_Add->setDisabled(false);
				return (true);
			case (Event::EventType::FriendAdded):
				Button_Add->setDisabled(false);
				return (true);
			default:
				return (false);
			}
		}
	}
