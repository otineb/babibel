#include "include/client/Graphical/QWindows/MainWindows.hpp"

	namespace Graphical
	{
		MainWindows::MainWindows() :
			QMainWindow(),
			_Width(960), _Height(1024)
		{
			InitQtObject();
		}

		MainWindows::MainWindows(int Width, int Height) :
			QMainWindow(),
			_Width(Width), _Height(Height)
		{
			InitQtObject();
		}

		void			MainWindows::InitQtObject()
		{
			setFixedSize(_Width, _Height);
		}

		MainWindows::~MainWindows()
		{
		}

		bool				MainWindows::event(QEvent *event)
		{
			return (QWidget::event(event));
		}

		void				MainWindows::ShowWindows()
		{
			show();
			_showed = true;
		}
		
		void				MainWindows::HideWindows()
		{
			hide();
			_showed = false;
		}

		WindowType			MainWindows::Type() const { return (WindowType::MainWindow); }

		bool				MainWindows::TreatEvent(Event::Event const &)
		{
			return (false);
		}
	}