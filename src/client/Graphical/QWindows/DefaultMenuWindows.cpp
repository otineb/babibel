#include "include/client/Client.hpp"

#include "include/client/Graphical/QWindows/DefaultMenuWindows.hpp"
#include "include/client/Event/EventManager.hpp"

	namespace Graphical
	{
		DefaultMenuWindows::DefaultMenuWindows(QWidget & Windows)
			: QWidget(&Windows)
		{
			this->setupUi();
			this->retranslateUi();
			this->setupConnectSlot();
		}

		DefaultMenuWindows::~DefaultMenuWindows()
		{
		}

		void									DefaultMenuWindows::setupConnectSlot()
		{
			connect(Button_Call, SIGNAL(clicked()), this, SLOT(Pressed_Call()));
			connect(Button_UnFriend, SIGNAL(clicked()), this, SLOT(Pressed_UnFriend()));
			connect(Button_AddFriend, SIGNAL(clicked()), this, SLOT(Pressed_AddFriend()));
		}

		void									DefaultMenuWindows::setupUi()
		{
			if (this->objectName().isEmpty())
				this->setObjectName(QStringLiteral("Window"));
			this->resize(480, 750);
			this->setMinimumSize(QSize(480, 750));
			this->setMaximumSize(QSize(480, 1024));

			verticalLayout = new QVBoxLayout(this);
			verticalLayout->setSpacing(15);
			verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
			verticalLayout->setContentsMargins(0, 0, 15, 0);
			Label_Name = new QLabel(this);
			Label_Name->setObjectName(QStringLiteral("Label_Name"));
			Label_Name->setMinimumSize(QSize(480, 35));
			Label_Name->setMaximumSize(QSize(480, 35));
			QFont font;
			font.setPointSize(15);
			Label_Name->setFont(font);
			Label_Name->setLayoutDirection(Qt::LeftToRight);
			Label_Name->setAlignment(Qt::AlignHCenter | Qt::AlignTop);

			verticalLayout->addWidget(Label_Name);

			List_Contact = new QListWidget(this);
			List_Contact->setObjectName(QStringLiteral("List_Contact"));
			List_Contact->setEnabled(true);
			List_Contact->setMinimumSize(QSize(480, 25));
			List_Contact->setMaximumSize(QSize(480, 1024));
			List_Contact->setFont(font);
			List_Contact->setFrameShadow(QFrame::Sunken);
			List_Contact->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
			List_Contact->setSelectionBehavior(QAbstractItemView::SelectItems);
			List_Contact->setIconSize(QSize(0, 0));
			List_Contact->setMovement(QListView::Static);
			List_Contact->setFlow(QListView::TopToBottom);
			List_Contact->setLayoutMode(QListView::SinglePass);
			List_Contact->setGridSize(QSize(0, 40));
			List_Contact->setViewMode(QListView::ListMode);
			List_Contact->setUniformItemSizes(false);
			List_Contact->setWordWrap(false);
			List_Contact->setSortingEnabled(false);

			verticalLayout->addWidget(List_Contact);

			Layout_Buttons = new QHBoxLayout();
			Layout_Buttons->setObjectName(QStringLiteral("Layout_Buttons"));
			Button_UnFriend = new QPushButton(this);
			Button_UnFriend->setObjectName(QStringLiteral("Button_UnFriend"));

			Layout_Buttons->addWidget(Button_UnFriend);

			Button_Call = new QPushButton(this);
			Button_Call->setObjectName(QStringLiteral("Button_Call"));

			Layout_Buttons->addWidget(Button_Call);


			verticalLayout->addLayout(Layout_Buttons);

			Button_AddFriend = new QPushButton(this);
			Button_AddFriend->setObjectName(QStringLiteral("Button_AddFriend"));

			verticalLayout->addWidget(Button_AddFriend);

			List_Contact->setCurrentRow(-1);
		}

		void									DefaultMenuWindows::retranslateUi()
		{
			this->setWindowTitle(QApplication::translate("Window", "Window", Q_NULLPTR));
			Label_Name->setText(QApplication::translate("Window", "NAME_OF_USER", Q_NULLPTR));

			Button_UnFriend->setText(QApplication::translate("Window", "Unfriend", Q_NULLPTR));
			Button_Call->setText(QApplication::translate("Window", "Call", Q_NULLPTR));
			Button_AddFriend->setText(QApplication::translate("Window", "Add a Friend", Q_NULLPTR));
		}

		void									DefaultMenuWindows::updateValue()
		{
			std::string	const &					name = Client::Client::GetSingleton()->GetClientName();
			std::vector<std::string> const &	friendList = Client::Client::GetSingleton()->GetClientFriendList();
			
			Label_Name->setText(QString::fromStdString(name));

			List_Contact->clear();

			for (std::string friendName : friendList)
			{
				QListWidgetItem *item = new QListWidgetItem(List_Contact);
				item->setText(QString::fromStdString(friendName));
			}
		}

		WindowType								DefaultMenuWindows::Type() const { return (WindowType::DefaultMenu); }

		void									DefaultMenuWindows::ShowWindows()
		{
			this->updateValue();

			this->show();
			_showed = true;
		}

		void									DefaultMenuWindows::HideWindows()
		{
			this->hide();
			_showed = false;
		}

		void									DefaultMenuWindows::Pressed_AddFriend()
		{
			Event::EventManager::Singleton_AddEvent(Event::EventType::AddAFriend);
		}

		void									DefaultMenuWindows::Pressed_Call()
		{
			auto currentItem = List_Contact->currentItem();

			if (currentItem != NULL)
			{
				std::vector<std::string> data;

				data.push_back(currentItem->text().toStdString());

				Event::EventManager::Singleton_AddEvent(Event::EventType::Calling, data);

				List_Contact->setCurrentRow(-1);
			}
		}

		void									DefaultMenuWindows::Pressed_UnFriend()
		{
			auto currentItem = List_Contact->currentItem();

			if (currentItem != NULL)
			{
				std::vector<std::string> data;
				
				data.push_back(currentItem->text().toStdString());

				Event::EventManager::Singleton_AddEvent(Event::EventType::UnFriend, data);
			
				List_Contact->setCurrentRow(-1);
			}
		}

		bool									DefaultMenuWindows::TreatEvent(Event::Event const &event)
		{
			switch (event.Type())
			{
			case (Event::EventType::Calling):
			case (Event::EventType::ReceiveCall):
				Button_Call->setDisabled(true);
				return (true);

			case (Event::EventType::CallRefused):
			case (Event::EventType::CallEnd):
				Button_Call->setDisabled(false);
				return (true);
				case (Event::EventType::UpdateContactList):
					this->updateValue();
			default:
				return (false);
			}
		}
	}