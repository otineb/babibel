# include "include/client/Graphical/QWindows/InscriptionWindows.hpp"

# include "include/client/Event/EventManager.hpp"

	namespace Graphical
	{
		InscriptionWindows::InscriptionWindows(QWidget & Windows)
			: QWidget(&Windows)
		{
			this->setupUi();
			this->retranslateUi();
			this->setupConnectSlot();
		}

		InscriptionWindows::~InscriptionWindows()
		{
			//delete verticalLayoutWidget;
			//delete Form;
			//delete Label_UserName;
			//delete Input_Username;
			//delete Label_Password;
			//delete Input_Password;
			//delete Label_Title;
		}

		void							InscriptionWindows::setupConnectSlot()
		{
			connect(Button_Connect, SIGNAL(clicked()), this, SLOT(Pressed_Connect()));
			connect(Button_CreateAccount, SIGNAL(clicked()), this, SLOT(Pressed_CreateAccount()));
		}

		void							InscriptionWindows::setupUi()
		{
			if (this->objectName().isEmpty())
				this->setObjectName(QStringLiteral("this"));
			this->resize(480, 750);
			this->move(240, 100);
			this->setMinimumSize(QSize(480, 750));
			this->setMaximumSize(QSize(480, 750));
			verticalLayout = new QVBoxLayout(this);
			verticalLayout->setSpacing(0);
			verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
			verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
			verticalLayout->setContentsMargins(0, 0, 0, 0);
			Label_Title = new QLabel(this);
			Label_Title->setObjectName(QStringLiteral("Label_Title"));
			Label_Title->setMaximumSize(QSize(480, 270));
			Label_Title->setPixmap(QPixmap(QString::fromUtf8(":/Images/logo-small.png")));
			Label_Title->setScaledContents(true);
			Label_Title->setAlignment(Qt::AlignCenter);

			verticalLayout->addWidget(Label_Title);

			Label_UserName = new QLabel(this);
			Label_UserName->setObjectName(QStringLiteral("Label_UserName"));
			Label_UserName->setMaximumSize(QSize(480, 480));
			QFont font;
			font.setPointSize(10);
			Label_UserName->setFont(font);
			Label_UserName->setAlignment(Qt::AlignBottom | Qt::AlignLeading | Qt::AlignLeft);

			verticalLayout->addWidget(Label_UserName);

			Input_Username = new QLineEdit(this);
			Input_Username->setObjectName(QStringLiteral("Input_Username"));
			Input_Username->setMaximumSize(QSize(480, 25));
			QFont font1;
			font1.setPointSize(9);
			Input_Username->setFont(font1);

			verticalLayout->addWidget(Input_Username);

			Label_Password = new QLabel(this);
			Label_Password->setObjectName(QStringLiteral("Label_Password"));
			Label_Password->setMaximumSize(QSize(480, 480));
			Label_Password->setFont(font);
			Label_Password->setAlignment(Qt::AlignBottom | Qt::AlignLeading | Qt::AlignLeft);

			verticalLayout->addWidget(Label_Password);

			Input_Password = new QLineEdit(this);
			Input_Password->setObjectName(QStringLiteral("Input_Password"));
			Input_Password->setMaximumSize(QSize(480, 25));
			Input_Password->setFont(font1);

			verticalLayout->addWidget(Input_Password);

			Label_Error = new QLabel(this);
			Label_Error->setObjectName(QStringLiteral("Label_Error"));
			Label_Error->setMinimumSize(QSize(480, 25));
			Label_Error->setMaximumSize(QSize(480, 25));
			Label_Error->setStyleSheet(QStringLiteral("color: rgb(220, 50, 50);"));
			Label_Error->setAlignment(Qt::AlignCenter);

			verticalLayout->addWidget(Label_Error);

			Box_Connect = new QHBoxLayout();
			Box_Connect->setSpacing(0);
			Box_Connect->setObjectName(QStringLiteral("Box_Connect"));
			Button_Connect = new QPushButton(this);
			Button_Connect->setObjectName(QStringLiteral("Button_Connect"));
			Button_Connect->setMaximumSize(QSize(250, 30));
			Button_Connect->setLayoutDirection(Qt::LeftToRight);

			Box_Connect->addWidget(Button_Connect);


			verticalLayout->addLayout(Box_Connect);

			Box_CreateAccount = new QHBoxLayout();
			Box_CreateAccount->setSpacing(0);
			Box_CreateAccount->setObjectName(QStringLiteral("Box_CreateAccount"));
			Button_CreateAccount = new QPushButton(this);
			Button_CreateAccount->setObjectName(QStringLiteral("Button_CreateAccount"));
			Button_CreateAccount->setMaximumSize(QSize(250, 30));
			Button_CreateAccount->setAutoDefault(false);

			Box_CreateAccount->addWidget(Button_CreateAccount);


			verticalLayout->addLayout(Box_CreateAccount);

		}

		void							InscriptionWindows::retranslateUi()
		{
			this->setWindowTitle(QApplication::translate("MainWidget", "Form", Q_NULLPTR));
			Label_Title->setText(QString());

			Label_UserName->setText(QApplication::translate("MainWidget", "Username", Q_NULLPTR));
			Input_Username->setText(QString());
			
			Label_Password->setText(QApplication::translate("MainWidget", "Password", Q_NULLPTR));
			Input_Password->setText(QString());

			Label_Error->setText(QString());

			Button_Connect->setText(QApplication::translate("MainWidget", "Connect", Q_NULLPTR));
			Button_CreateAccount->setText(QApplication::translate("MainWidget", "Register", Q_NULLPTR));
		}

		std::vector<std::string>		InscriptionWindows::GetConnectionData() const
		{
			std::vector<std::string>	string;

			std::string username = this->Input_Username->text().toStdString();
			std::string password = this->Input_Password->text().toStdString();

			if (username.length() == 0 || password.length() == 0)
				return (string);

			string.push_back(username);
			string.push_back(password);

			return (string);
		}

		void							InscriptionWindows::Pressed_Connect()
		{
			std::vector<std::string>	connectionData = this->GetConnectionData();

			if (connectionData.size() == 0)
				return;

			Button_Connect->setDisabled(true);
			Button_CreateAccount->setDisabled(true);

			Event::EventManager::Singleton_AddEvent(Event::EventType::Connect, connectionData);
		}
		void							InscriptionWindows::Pressed_CreateAccount()
		{
			std::vector<std::string>	connectionData = this->GetConnectionData();

			if (connectionData.size() == 0)
				return;

			Button_Connect->setDisabled(true);
			Button_CreateAccount->setDisabled(true);

			Event::EventManager::Singleton_AddEvent(Event::EventType::CreateAccount, connectionData);
		}
		WindowType						InscriptionWindows::Type() const { return (WindowType::Inscription); }

		void							InscriptionWindows::ShowWindows()
		{
			this->show();
			_showed = true;
		}

		void							InscriptionWindows::HideWindows()
		{
			this->hide();
			_showed = false;

			retranslateUi();
		}

		bool							InscriptionWindows::TreatEvent(Event::Event const &event)
		{
			switch (event.Type())
			{
			case (Event::EventType::AccountRefuse):
				Label_Error->setText(QString::fromStdString("Error: Can't create an account: " + event.Data()[0]));
				Button_Connect->setDisabled(false);
				Button_CreateAccount->setDisabled(false);
				return (true);
			case (Event::EventType::AccountAccept):
				Button_Connect->setDisabled(false);
				Button_CreateAccount->setDisabled(false);
				return (true);
			case (Event::EventType::ConnectionRef):
				Label_Error->setText(QString::fromStdString("Error: Can't connect: " + event.Data()[0]));
				Button_Connect->setDisabled(false);
				Button_CreateAccount->setDisabled(false);
				return (true);
			case (Event::EventType::ConnectionAcc):
				Button_Connect->setDisabled(false);
				Button_CreateAccount->setDisabled(false);
				return (true);
			default:
				return (false);
			}
		}
	}
