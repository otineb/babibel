#include "include/client/Client.hpp"

#include "include/client/Graphical/QWindows/InCallWindows.hpp"
#include "include/client/Event/EventManager.hpp"

	namespace Graphical
	{
		InCallWindows::InCallWindows(QWidget & Windows)
			: QWidget(&Windows)
		{
			this->setupUi();
			this->retranslateUi();
			this->setupConnectSlot();
		}

		InCallWindows::~InCallWindows()
		{
		}

		void							InCallWindows::setupConnectSlot()
		{
			connect(Button_HangUp, SIGNAL(clicked()), this, SLOT(Pressed_HangUp()));
		}

		void							InCallWindows::setupUi()
		{
			if (this->objectName().isEmpty())
				this->setObjectName(QStringLiteral("Form"));
			this->resize(480, 480);
			this->move(480, 0);

			this->setMinimumSize(QSize(480, 480));
			this->setMaximumSize(QSize(480, 480));
			
			verticalLayout = new QVBoxLayout(this);
			verticalLayout->setSpacing(0);
			verticalLayout->setObjectName(QStringLiteral("verticalLayout_2"));
			verticalLayout->setContentsMargins(40, 0, 40, 0);
			Label_NameOfCaller = new QLabel(this);
			Label_NameOfCaller->setObjectName(QStringLiteral("Label_NameOfCaller"));
			Label_NameOfCaller->setMinimumSize(QSize(400, 40));
			Label_NameOfCaller->setMaximumSize(QSize(400, 40));
			QFont font;
			font.setPointSize(15);
			Label_NameOfCaller->setFont(font);
			Label_NameOfCaller->setAlignment(Qt::AlignCenter);

			verticalLayout->addWidget(Label_NameOfCaller);

			Label_CallLength = new QLabel(this);
			Label_CallLength->setObjectName(QStringLiteral("Label_CallLength"));
			Label_CallLength->setMinimumSize(QSize(400, 25));
			Label_CallLength->setMaximumSize(QSize(400, 25));
			Label_CallLength->setAlignment(Qt::AlignCenter);

			verticalLayout->addWidget(Label_CallLength);

			Button_HangUp = new QPushButton(this);
			Button_HangUp->setObjectName(QStringLiteral("Button_HangUp"));
			Button_HangUp->setMinimumSize(QSize(400, 50));
			Button_HangUp->setMaximumSize(QSize(400, 50));

			verticalLayout->addWidget(Button_HangUp);
		}

		void							InCallWindows::retranslateUi()
		{
			this->setWindowTitle(QApplication::translate("Form", "Form", Q_NULLPTR));
			Label_NameOfCaller->setText(QApplication::translate("Form", "CALLER_NAME", Q_NULLPTR));

			Label_CallLength->setText(QApplication::translate("Form", "00:00", Q_NULLPTR));
			Button_HangUp->setText(QApplication::translate("Form", "Hang Up", Q_NULLPTR));
		}

		void							InCallWindows::updateValue()
		{
			std::string	const &					name = Client::Client::GetSingleton()->GetCallerName();
			
			Label_CallLength->setText(QApplication::translate("Form", "00:00", Q_NULLPTR));
			Label_NameOfCaller->setText(QString::fromStdString(name));
		}

		WindowType						InCallWindows::Type() const { return (WindowType::InCall); }

		void							InCallWindows::ShowWindows()
		{
			this->updateValue();

			this->show();
			_showed = true;
		}

		void							InCallWindows::HideWindows()
		{
			this->hide();
			_showed = false;
		}

		void							InCallWindows::Pressed_HangUp()
		{
			Event::EventManager::Singleton_AddEvent(Event::EventType::HangUp);
		}

		bool							InCallWindows::TreatEvent(Event::Event const &)
		{
			return (false);
		}
	}