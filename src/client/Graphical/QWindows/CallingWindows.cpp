#include "include/client/Client.hpp"

#include "include/client/Graphical/QWindows/CallingWindows.hpp"
#include "include/client/Event/EventManager.hpp"

	namespace Graphical
	{
		CallingWindows::CallingWindows(QWidget & Windows)
			: QWidget(&Windows)
		{
			this->setupUi();
			this->retranslateUi();
			this->setupConnectSlot();
		}

		CallingWindows::~CallingWindows()
		{
		}

		void							CallingWindows::setupConnectSlot()
		{
			connect(Button_HangUp, SIGNAL(clicked()), this, SLOT(Pressed_HangUp()));
			connect(Button_Respond, SIGNAL(clicked()), this, SLOT(Pressed_Respond()));
		}

		void							CallingWindows::setupUi()
		{
			if (this->objectName().isEmpty())
			    this->setObjectName(QStringLiteral("Form"));
			this->resize(480, 480);
			this->move(480, 0);

			this->setMinimumSize(QSize(480, 480));
			this->setMaximumSize(QSize(480, 480));
			this->setStyleSheet("background-image: url(:/Images/ressource/Images/Background.png);");

			verticalLayout = new QVBoxLayout(this);
			verticalLayout->setSpacing(15);
			verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
			verticalLayout->setContentsMargins(40, 0, 40, 0);
			Label_NameOfCaller = new QLabel(this);
			Label_NameOfCaller->setObjectName(QStringLiteral("Label_NameOfCaller"));
			Label_NameOfCaller->setMinimumSize(QSize(400, 40));
			Label_NameOfCaller->setMaximumSize(QSize(400, 40));
			QFont font;
			font.setPointSize(15);
			Label_NameOfCaller->setFont(font);
			Label_NameOfCaller->setAlignment(Qt::AlignCenter);

			verticalLayout->addWidget(Label_NameOfCaller);

			Button_Respond = new QPushButton(this);
			Button_Respond->setObjectName(QStringLiteral("Button_Respond"));
			Button_Respond->setMinimumSize(QSize(400, 50));
			Button_Respond->setMaximumSize(QSize(400, 50));
				
			verticalLayout->addWidget(Button_Respond);
				
		    Button_HangUp = new QPushButton(this);
		    Button_HangUp->setObjectName(QStringLiteral("Button_HangUp"));
		    Button_HangUp->setMinimumSize(QSize(400, 50));
		    Button_HangUp->setMaximumSize(QSize(400, 50));

		    verticalLayout->addWidget(Button_HangUp);
		}

		void							CallingWindows::retranslateUi()
		{
			this->setWindowTitle(QApplication::translate("Form", "Form", Q_NULLPTR));
			Label_NameOfCaller->setText(QApplication::translate("Form", "CALLER_NAME", Q_NULLPTR));
			Button_Respond->setText(QApplication::translate("Form", "Respond", Q_NULLPTR));
			Button_HangUp->setText(QApplication::translate("Form", "Hang Up", Q_NULLPTR));
		}

		void							CallingWindows::updateValue()
		{
			std::string	const &					name = Client::Client::GetSingleton()->GetCallerName();
			
			Label_NameOfCaller->setText(QString::fromStdString(name));
		}

			WindowType						CallingWindows::Type() const { return (WindowType::Calling); }

		void							CallingWindows::ShowWindows()
		{
			this->updateValue();

			this->show();
			_showed = true;
		}

		void							CallingWindows::HideWindows()
		{
			this->hide();
			_showed = false;
		}

		void							CallingWindows::Pressed_HangUp()
		{
			Event::EventManager::Singleton_AddEvent(Event::EventType::CallRefused);
		}

		void							CallingWindows::Pressed_Respond()
		{
			Event::EventManager::Singleton_AddEvent(Event::EventType::CallAccepted);
		}

		bool							CallingWindows::TreatEvent(Event::Event const &)
		{
			return (false);
		}
	}