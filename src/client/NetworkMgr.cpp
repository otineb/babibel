//
// NetworkMgr.cpp for NetworkMgr.cpp in /home/daze/rendu/c++/babibel
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Sat Sep 30 11:42:20 2017 DaZe
// Last update Wed Oct  4 16:26:55 2017 Myriam
//

#include "include/client/NetworkMgr.hpp"

NetworkMgr::NetworkMgr()
{
  _tcpSocket = new TcpSocket;
}

TcpSocket		*NetworkMgr::getTcpSocket() const
{
  return (_tcpSocket);
}

void			NetworkMgr::connectUdp(std::string const &ipAddress)
{
  _udpSocket = new UdpSocket(ipAddress);
}

void            NetworkMgr::disconnectUdp()
{
  delete _udpSocket;
}
