//
// Created by daze on 10/5/17.
//

#include <iostream>
#include "client/ResponseHandler.hpp"

Event::EventType ResponseHandler::calling(QByteArray const &)
{
    return (Event::EventType::ReceiveCall);
}

Event::EventType ResponseHandler::hangUp(QByteArray const &)
{
    return (Event::EventType::CallEnd);
}

Event::EventType ResponseHandler::timeOut(QByteArray const &)
{
    return (Event::EventType::Timeout);
}

Event::EventType ResponseHandler::connection(QByteArray const &array)
{
    const unsigned short *code = reinterpret_cast<const unsigned short *>(array.data());

    if (*code == 1)
        return (Event::EventType::ConnectionAcc);
    else
        return (Event::EventType::ConnectionRef);
}

Event::EventType ResponseHandler::registration(QByteArray const &array)
{
      const unsigned short *code = reinterpret_cast<const unsigned short *>(array.data());

    if (*code == 1)
        return (Event::EventType::AccountAccept);
      else
        return (Event::EventType::AccountRefuse);
}

std::unordered_map<ActionType, std::function<Event::EventType(QByteArray const&)>> const& ResponseHandler::getMap() const
{
    return (_map);
}

Event::EventType ResponseHandler::callAccepted(QByteArray const &)
{
    return (Event::EventType::CallBegin);
}

Event::EventType ResponseHandler::addContact(QByteArray const &array)
{
    const unsigned short *code = reinterpret_cast<const unsigned short *>(array.data());

    if (*code == 1)
        return (Event::EventType::FriendAdded);
    return (Event::EventType::FriendAddErr);
}

Event::EventType ResponseHandler::disconnection(QByteArray const &)
{
    return (Event::EventType::Disconnection);
}

Event::EventType ResponseHandler::updateContactList(QByteArray const &)
{
    return (Event::EventType::UpdateContactList);
}

Event::EventType ResponseHandler::acceptSession(QByteArray const &)
{
    return (Event::EventType::CallBegin);
}