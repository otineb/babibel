//
// Created by mymy on 30/09/17.
//

#include "server/ASystem.hpp"
#include "server/Chief/ChiefLeader.hpp"

babelServer::ASystem::ASystem(ChiefLeader *chief) :
_chief(chief)
{

}

babelServer::ASystem::ASystem() : _chief(nullptr)
{}

void babelServer::ASystem::sendEvent(struct Event const& event)
{
  this->_chief->receiveEvent(event);
}

void babelServer::ASystem::setChief(ChiefLeader *c)
{
  _chief = c;
}