#include <iostream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "server/Database/DatabaseMgr.hpp"
#include "server/Chief/ChiefLeader.hpp"
#include "server/Network/NetworkManager.hpp"

int 	main()
{
  //DatabaseMgr	databaseMgr;

  //databaseMgr.registerUser("yolanda", "pipi");
  //databaseMgr.addFriend("yolanda", "prout");
    try
    {
        boost::asio::io_service io_service;
        babelServer::NetworkManager server(io_service);
    	babelServer::ChiefLeader chiefLeader(server);
        server.setChief(&chiefLeader);
        server.start_accept();
        io_service.run();
    }
    catch (std::exception const& e)
    {
        std::cout << e.what() << std::endl;
    }
    return 0;
}
