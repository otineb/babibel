//
// Created by benito on 27/09/17.
//

#include <include/client/ActionType.hpp>
#include "server/Network/TcpConnexion.hpp"
#include "server/Chief/ChiefLeader.hpp"

babelServer::TcpConnexion::TcpConnexion(boost::asio::io_service& io_service, ChiefLeader *c) : ASystem(c), socket_(io_service)
{
}

std::shared_ptr<babelServer::TcpConnexion> babelServer::TcpConnexion::create(boost::asio::io_service& io_service, ChiefLeader *c)
{
    return std::shared_ptr<TcpConnexion>(new TcpConnexion(io_service, c));
}

void babelServer::TcpConnexion::getEvent(struct Event const *) {}

boost::asio::ip::tcp::socket& babelServer::TcpConnexion::socket()
{
    return socket_;
}

void    babelServer::TcpConnexion::writeMessage(Packet const& p)
{
    mutie_.lock();
    /* blocage/debloquage ?*/
    packetQueue_.push(p);
    if (packetQueue_.size() == 1) {
        writtenPacket_ = p;

        boost::asio::async_write(socket_, boost::asio::buffer(writtenPacket_.getData(),
                                                              sizeof(writtenPacket_._size) + writtenPacket_._size),
                                 boost::bind(&TcpConnexion::handleWrite,
                                             this->shared_from_this(),
                                             boost::asio::placeholders::error,
                                             boost::asio::placeholders::bytes_transferred));
    }
    mutie_.unlock();
}

/*
void         babelServer::TcpConnexion::handleWriteHeader(const boost::system::error_code& error,
                                                    size_t bytes_transferred)
{
    mutie.lock();
    blocage/debloquage ?
    std::cout << "j'écris donc " << writtenPacket_.getData() << " et " << writtenPacket_.getData() + 2 << " ce qui est de taille " << writtenPacket_._size << std::endl;
    boost::asio::async_write(socket_, boost::asio::buffer(writtenPacket_.getData(), writtenPacket_._size),
                             boost::bind(&TcpConnexion::handleWriteBody,
                                         this->shared_from_this(),
                                         boost::asio::placeholders::error,
                                         boost::asio::placeholders::bytes_transferred));
    mutie.unlock();
}
*/

void babelServer::TcpConnexion::handleWrite(const boost::system::error_code &ec, size_t)
{
    if (!ec) {
        mutie_.lock();
        packetQueue_.pop();
        if (packetQueue_.size() != 0) {
            writtenPacket_ = packetQueue_.front();

            boost::asio::async_write(socket_, boost::asio::buffer(writtenPacket_.getData(),
                                                                  sizeof(writtenPacket_._size) + writtenPacket_._size),
                                     boost::bind(&TcpConnexion::handleWrite,
                                                 this->shared_from_this(),
                                                 boost::asio::placeholders::error,
                                                 boost::asio::placeholders::bytes_transferred));
        }
        mutie_.unlock();
    }
    else
    {
        std::shared_ptr<TcpConnexion> x = shared_from_this();
        disconnect();
    }
}

void    babelServer::TcpConnexion::start()
{
    packet_.setIp(socket_.remote_endpoint().address());

     boost::asio::async_read(socket_,
          boost::asio::buffer(&packet_._size, Packet::headerSize),
          boost::bind(&TcpConnexion::handleReadHeader, shared_from_this(),
            boost::asio::placeholders::error));
}

void babelServer::TcpConnexion::shutdown()
{
    boost::system::error_code ec;
    socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_send, ec);
    disconnect();
    if (ec)
        std::cout << "log : " << ec.message() << std::endl;
}

void babelServer::TcpConnexion::handleReadHeader(const boost::system::error_code &ec)
{
    if (!ec)
    {
        if (packet_._size > packet_.maxPacketSize)
        {
            std::cout << "log : this client tried to send a huge packet. Disconnection... -> "
                      << this->getIp().to_string() << std::endl;
            this->shutdown();
        }
        else
        {
            boost::asio::async_read(socket_,
                                    boost::asio::buffer(packet_.getData(), packet_._size),
                                    boost::bind(&TcpConnexion::handleReadBody,
                                                shared_from_this(),
                                                boost::asio::placeholders::error));
        }
    }
    else
    {
        std::cout << "log : " << ec.message() << " -> " << this->getIp().to_string() << std::endl;
        disconnect();
    }
}

void babelServer::TcpConnexion::disconnect()
{
    Event   event;

    event.actionType = ActionType::DISCONNECTION;
    event.ipAddressUser = getIp();
    socket_.close();
    this->_chief->receiveEvent(event);
}

void babelServer::TcpConnexion::handleReadBody(const boost::system::error_code &ec)
{
    if (!ec)
    {
        _chief->createActions(packet_);
        packet_.clean();
        boost::asio::async_read(socket_,
                                boost::asio::buffer(&packet_._size, Packet::headerSize),
                                boost::bind(&TcpConnexion::handleReadHeader, shared_from_this(),
                                            boost::asio::placeholders::error));
    }
    else
    {
        disconnect();
    }
}

boost::asio::ip::address const& babelServer::TcpConnexion::getIp() const
{
    return (this->packet_.getIpAddressUser());
}

babelServer::TcpConnexion::~TcpConnexion()
{
}