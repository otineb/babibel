//
// Created by mymy on 29/09/17.
//

#include "server/Network/Packet.hpp"

const unsigned int babelServer::Packet::headerSize = 4;

babelServer::Packet::Packet()
{
    _size = 512;
    this->clean();
}

void babelServer::Packet::setIp(boost::asio::ip::address const &ip)
{
    _ipAddressUser = ip;
}

boost::asio::ip::address const 	&babelServer::Packet::getIpAddressUser() const
{
    return (this->_ipAddressUser);
}

void babelServer::Packet::clean()
{
    std::memset(_data, 0, _size);
    _size = 0;
}

const char* babelServer::Packet::getData() const
{
    return (this->_data);
}

char 	*babelServer::Packet::getData()
{
  return (this->_data);
}

int babelServer::Packet::getSize() const
{
  return (this->_size);
}