//
// Created by benito on 27/09/17.
//

#include "server/Network/NetworkManager.hpp"
#include "server/Chief/ChiefLeader.hpp"

std::vector<babelServer::ActionType > babelServer::NetworkManager::handledActions =
        {ActionType::CALLING, ActionType::HANGUP, ActionType::RECEIVE_INVIT, ActionType::TIMEOUT,
         ActionType::CALLING_ACCEPTED, ActionType::CALLING_DECLINED, ActionType::UPDATE_CONTACT_LIST};

babelServer::NetworkManager::NetworkManager(boost::asio::io_service &io_service) :
        acceptor_(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 1024))
{}

void babelServer::NetworkManager::writePacket(Packet const &p)
{
    //std::cout << "je vais ecire un packet" << std::endl;
    auto const& ip = p.getIpAddressUser();
    auto idx = std::find_if(clients_.begin(), clients_.end(),
              [ip](std::shared_ptr<TcpConnexion> const& t)
        {
            //std::cout << "POINT SENSIBLE" << std::endl;
            //std::cout << t->getIp().to_string() << " == " << ip.to_string() << std::endl;
            return (t->getIp() == ip);
        });
    //std::cout << "quel point sensible ?" << std::endl;
    if (idx != clients_.end())
        (*idx)->writeMessage(p);
}

void babelServer::NetworkManager::destroy(struct Event const *e)
{
    auto const& ip = e->ipAddressUser;
    auto idx = std::find_if(clients_.begin(), clients_.end(),
                            [ip](std::shared_ptr<TcpConnexion> const& t)
                            {
                                return (t->getIp() == ip);
                            });
    if (idx != clients_.end())
        clients_.erase(idx);
}

void babelServer::NetworkManager::getEvent(struct Event const *e)
{
    if (e->actionType == ActionType::DISCONNECTION)
    {
        this->destroy(e);
    }
    else if (dynamic_cast<const struct ResponseEvent *>(e) ||
            (std::binary_search(handledActions.begin(), handledActions.end(), e->actionType)))
    {
        Packet packet;
        this->_chief->translateActions(packet, e);
        writePacket(packet);
    }
}

void        babelServer::NetworkManager::start_accept()
{
    std::shared_ptr<TcpConnexion> new_connection = TcpConnexion::create(acceptor_.get_io_service(), _chief);

    acceptor_.async_accept(new_connection->socket(),
                           boost::bind(&NetworkManager::handle_accept, this, new_connection,
                                       boost::asio::placeholders::error));
}

void        babelServer::NetworkManager::handle_accept(std::shared_ptr<TcpConnexion> const& new_connection,
                   const boost::system::error_code& error)
{
    if (!error)
    {
        new_connection->start();
    }
    clients_.push_back(new_connection);

    start_accept();
}
