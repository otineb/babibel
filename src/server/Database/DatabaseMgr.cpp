//
// Created by mymy on 27/09/17.
//

#include <iostream>
#include <algorithm>
#include <boost/property_tree/json_parser.hpp>
#include "server/Database/DatabaseMgr.hpp"
#include "server/Event/Event.hpp"
#include "server/Event/ActionType.hpp"

babelServer::DatabaseMgr::DatabaseMgr(ChiefLeader *chief) :
	ASystem(chief), _actionFunctions() ,_users(), _contactLists(), _root(), _contactRoot()
{
  this->initActionFunction();
  std::ifstream f("../logs.txt");

  if (!f.is_open())
    return;

  boost::property_tree::read_json("../logs.txt", _root);
  for (boost::property_tree::ptree::value_type &user : _root.get_child("Users"))
    {
      User newUser(user.first, user.second.data());

      _users.push_back(newUser);
    }
  f.close();

  f.open("../contact_lists.txt");
  if (!f.is_open())
    throw std::runtime_error("Logs file without contactList file;");
  f.close();
  boost::property_tree::read_json("../contact_lists.txt", _contactRoot);
  unsigned int	idx = 0;
  for (auto &user : this->_users)
    {
      for (boost::property_tree::ptree::value_type &contact : _contactRoot.get_child(user.getPseudo()))
	  this->_contactLists[user.getPseudo()].push_back(contact.second.data());
      idx += 1;
    }
}

void babelServer::DatabaseMgr::initActionFunction()
{
  this->_actionFunctions.insert(std::make_pair(ActionType::REGISTER, std::bind(&DatabaseMgr::registerUser, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::CONNECTION, std::bind(&DatabaseMgr::connect, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::ADDCONTACT, std::bind(&DatabaseMgr::addFriend, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DELETECONTACT, std::bind(&DatabaseMgr::deleteFriend, this, std::placeholders::_1)));
    this->_actionFunctions.insert(std::make_pair(ActionType::DISCONNECTION, std::bind(&DatabaseMgr::disconnect, this, std::placeholders::_1)));

}

void	babelServer::DatabaseMgr::getEvent(struct Event const *event)
{
  //std::cout << "database get event" << std::endl;
  if (!dynamic_cast<const ResponseEvent *>(event) && this->_actionFunctions.find(event->actionType) != this->_actionFunctions.end())
    this->_actionFunctions[event->actionType](event);
  //std::cout << "fin de get event" << std::endl;
}

void 	babelServer::DatabaseMgr::registerUser(struct Event const *event)
{
    ResponseEvent newEvent(*event);

  if (this->isUserRegistered(event->data[0]))
    {
      newEvent.ret = ReturnValue::USERNAME_EXISTS;
      this->sendEvent(newEvent);
        return;
    }
  newEvent.ret = ReturnValue::OK;
  User newUser(event->data[0], event->data[1]);
  this->sendEvent(newEvent);

  std::mutex lizie;

  lizie.lock();
  this->_users.push_back(newUser);

  if (this->_users.size() == 1)
    {
      boost::property_tree::ptree userNode;
      std::ofstream 		ofs("../logs.txt");

      userNode.put(this->_users.back().getPseudo(), this->_users.back().getPassword());

      this->_root.add_child("Users", userNode);
      boost::property_tree::write_json(ofs, this->_root);

      ofs.close();
    }
  else
    {
      boost::property_tree::ptree userNode = this->_root.get_child("Users");

      for (auto &user : this->_users)
	userNode.put(user.getPseudo(), user.getPassword());
      std::ofstream ofs("../logs.txt",  std::ios::out | std::ios::trunc);

      this->_root.erase("Users");
      this->_root.add_child("Users", userNode);

      boost::property_tree::write_json(ofs, this->_root);
      ofs.close();
    }
  lizie.unlock();
  this->addContactList(newUser.getPseudo());
}

void 	babelServer::DatabaseMgr::addContactList(std::string const &pseudo)
{
  boost::property_tree::ptree contactNode;

  std::mutex benie;

  benie.lock();
  this->_contactRoot.add_child(pseudo, contactNode);

  std::ofstream ofs("../contact_lists.txt", std::ios::out | std::ios::trunc);
  boost::property_tree::write_json(ofs, this->_contactRoot);
  ofs.close();
  benie.unlock();
}

void 	babelServer::DatabaseMgr::addFriend(struct Event const *event)
{
  ResponseEvent	newEvent(*event);
  int 	idx;
  int 	idx_toAdd;

  if ((idx = findUserWithIp(event->ipAddressUser)) == -1 || !this->_users[idx].isConnected() || this->_users[idx].getPseudo() == event->data[0])
    newEvent.ret = ReturnValue::UNKNOWN_ERROR;
  else if (!isUserRegistered(event->data[0]))
    newEvent.ret = ReturnValue::INVALID_USERNAME;
  else if (isContact(this->_users[idx].getPseudo(), event->data[0]))
    newEvent.ret = ReturnValue::INVALID_USERNAME;
  else
	{
	  std::mutex mimie;

	  mimie.lock();
	  this->_contactLists[this->_users[idx].getPseudo()].push_back(event->data[0]);
	  mimie.unlock();

	  this->rewriteContactsFromUser(this->_users[idx].getPseudo());
	  newEvent.ret = ReturnValue::OK;

	  this->_contactLists[event->data[0]].push_back(this->_users[idx].getPseudo());
	  this->rewriteContactsFromUser(event->data[0]);
	  idx_toAdd = this->findUserWithName(event->data[0]);
	  if (this->_users[idx_toAdd].isConnected())
	    this->sendContactList(this->_users[idx_toAdd]);
	}
  this->sendEvent(newEvent);
  if (newEvent.ret == ReturnValue::OK)
    this->sendContactList(this->_users[idx]);
}

void 	babelServer::DatabaseMgr::deleteFriend(struct Event const *event)
{
  ResponseEvent	newEvent(*event);
  int 	idx;

  if ((idx = findUserWithIp(event->ipAddressUser)) == -1 || !this->_users[idx].isConnected())
    newEvent.ret = ReturnValue::UNKNOWN_ERROR;
  else if (!isUserRegistered(event->data[0]))
      newEvent.ret = ReturnValue::INVALID_USERNAME;
  else if (!isContact(this->_users[idx].getPseudo(), event->data[0]))
	newEvent.ret = ReturnValue::INVALID_USERNAME;
  else
	{
	  auto idToErase = std::find(this->_contactLists[this->_users[idx].getPseudo()].begin(),
			      this->_contactLists[this->_users[idx].getPseudo()].end(),
	  			event->data[0]);

	  std::mutex tomie;

	  tomie.lock();
	  this->_contactLists[this->_users[idx].getPseudo()].erase(idToErase);
	  tomie.unlock();

	  this->rewriteContactsFromUser(this->_users[idx].getPseudo());
	  newEvent.ret = ReturnValue::OK;

	  idToErase = std::find(this->_contactLists[event->data[0]].begin(),
				     this->_contactLists[event->data[0]].end(),
				     this->_users[idx].getPseudo());
	  this->_contactLists[event->data[0]].erase(idToErase);
	  this->rewriteContactsFromUser(event->data[0]);
	  int 	idxToDelete = findUserWithName(event->data[0]);
	  if (this->_users[idxToDelete].isConnected())
	    this->sendContactList(this->_users[idxToDelete]);
	}
  this->sendEvent(newEvent);
  if (newEvent.ret == ReturnValue::OK)
    this->sendContactList(this->_users[idx]);
}

void	babelServer::DatabaseMgr::rewriteContactsFromUser(std::string const &userConnected)
{
  boost::property_tree::ptree contactsNode;

  std::mutex paulie;

  paulie.lock();
  for (auto &contact : this->_contactLists[userConnected])
    {
      boost::property_tree::ptree contactNode;
      contactNode.put("", contact);
      contactsNode.push_back(std::make_pair("", contactNode));
    }

  this->_contactRoot.erase(userConnected);
  this->_contactRoot.add_child(userConnected, contactsNode);

  std::ofstream ofs("../contact_lists.txt",  std::ios::out | std::ios::trunc);
  boost::property_tree::write_json(ofs, this->_contactRoot);
  ofs.close();
  paulie.unlock();
}

bool	babelServer::DatabaseMgr::isContact(std::string const &user, std::string const &contact)
{
  for (auto &contactUser : this->_contactLists[user])
    {
      if (contactUser == contact)
	return (true);
    }
  return (false);
}

void	babelServer::DatabaseMgr::connect(Event const *event)
{
  ResponseEvent	newEvent(*event);

  for (auto &user : this->_users)
    {
      if (user.getPseudo() == event->data[0] && user.getPassword() == event->data[1])
	{
	  if (user.isConnected())
	    newEvent.ret = ReturnValue::UNKNOWN_ERROR;
	  else
	    {
	      std::mutex sexie;

	      sexie.lock();
	      user.setIpAddress(event->ipAddressUser);
	      user.connect();
	      sexie.unlock();

	      newEvent.ret = ReturnValue::OK;
	    }
	  this->sendEvent(newEvent);
	  if (newEvent.ret != ReturnValue::UNKNOWN_ERROR)
	  	this->sendContactList(user);
	  return;
	}
      else if (user.getPseudo() == event->data[0])
	  {
	    newEvent.ret = ReturnValue::INVALID_PWD;
	    this->sendEvent(newEvent);
	    return;
	  }
    }
  newEvent.ret = ReturnValue::INVALID_USERNAME;
  this->sendEvent(newEvent);
}

void    babelServer::DatabaseMgr::disconnect(struct Event const *event)
{
    int idx = this->findUserWithIp(event->ipAddressUser);

    if (idx != -1) {
        if (this->_users[idx].isConnected())
            this->_users[idx].disconnect();
    }
}

bool 	babelServer::DatabaseMgr::isUserRegistered(std::string const &pseudo) const
{
  for (auto &user : this->_users)
    if (user.getPseudo() == pseudo)
      return (true);
  return (false);
}

bool	babelServer::DatabaseMgr::isUserConnected(boost::asio::ip::address const &ip) const
{
  for (auto &user : this->_users)
    if (user.isConnected() && user.getIp() == ip)
      return (true);
  return (false);
}

bool	babelServer::DatabaseMgr::isUserConnected(std::string const &pseudo) const
{
  for (auto &user : this->_users)
    if (user.isConnected() && user.getPseudo() == pseudo)
      return (true);
  return (false);
}

std::string const &babelServer::DatabaseMgr::getPseudoByIP(boost::asio::ip::address const &ip) const
{
  for (auto &user : this->_users)
    {
      if (user.getIp() == ip)
	return (user.getPseudo());
    }
  throw std::runtime_error("Error : IP does not match any User.");
}

boost::asio::ip::address const &babelServer::DatabaseMgr::getIpByPseudo(std::string const &pseudo) const
{
  for (auto &user : this->_users)
    {
      if (user.getPseudo() == pseudo)
	return (user.getIp());
    }
  throw std::runtime_error("Error : Pseudo does not match any IP.");
}

int 	babelServer::DatabaseMgr::findUserWithIp(boost::asio::ip::address const &ip) const
{
  int 	idx = 0;

  for (auto &user : this->_users)
    {
      if (user.getIp() == ip)
    	return (idx);
      idx += 1;
    }
  return (-1);
}

int 	babelServer::DatabaseMgr::findUserWithName(std::string const &pseudo) const
{
  int 	idx = 0;

  for (auto &user : this->_users)
    {
      if (user.getPseudo() == pseudo)
	return (idx);
      idx += 1;
    }
  return (-1);
}


void 	babelServer::DatabaseMgr::sendContactList(User const &user)
{
  Event	newEvent;

  newEvent.ipAddressUser = user.getIp();
  newEvent.actionType = ActionType::UPDATE_CONTACT_LIST;

  std::mutex lilie;

  lilie.lock();
  for (auto &contact : this->_contactLists[user.getPseudo()])
    {
  	newEvent.data.push_back(contact);
    }
  lilie.unlock();

  this->sendEvent(newEvent);
}