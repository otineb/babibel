//
// Created by mymy on 28/09/17.
//

#include <iostream>
#include "server/Event/EventMgr.hpp"
#include "server/Event/Event.hpp"

babelServer::EventMgr::EventMgr() :
 _actionFunctions()
{
  this->_actionFunctions.insert(std::make_pair(ActionType::REGISTER, std::bind(&EventMgr::actionWithTwoArgs, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DELETION, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::CONNECTION, std::bind(&EventMgr::actionWithTwoArgs, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DISCONNECTION, std::bind(&EventMgr::actionWithNoneArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::GETCONTACTLIST, std::bind(&EventMgr::actionWithNoneArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::GETINVITATIONLIST, std::bind(&EventMgr::actionWithNoneArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::GETCONTACT, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::ADDCONTACT, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DELETECONTACT, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::ACCEPTCONTACT, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DECLINECONTACT, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::CHANGEPASSWORD, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::STARTSESSION, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::STOPSESSION, std::bind(&EventMgr::actionWithNoneArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::ACCEPTSESSION, std::bind(&EventMgr::actionWithNoneArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DECLINESESSION, std::bind(&EventMgr::actionWithNoneArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::ADDTOSESSION, std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
  this->_actionFunctions.insert(std::make_pair(ActionType::KICKFROMSESSION ,std::bind(&EventMgr::actionWithAnArg, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
}
babelServer::Event	*babelServer::EventMgr::translateInfo(Packet packet)
{
  Event *newEvent = new Event;
  ActionType action;

  newEvent->ipAddressUser = packet.getIpAddressUser();
  action = *reinterpret_cast<ActionType *>(packet.getData());
  if (this->_actionFunctions.find(action) == this->_actionFunctions.end())
    {
      /*sexe*/
      newEvent->actionType = ActionType::UNKNOWN;
    } else
    {
      newEvent->actionType = action;
      try
	{
	  this->_actionFunctions[action](*newEvent, packet.getData() + sizeof(unsigned short), packet._size);
	}
      catch (std::exception const &e)
	{
	  std::cout << e.what() << std::endl;
	  return (new ResponseEvent(*newEvent, ReturnValue::UNKNOWN_ERROR));
	  //probably a bad number of arguments
	}
    }
  return (newEvent);
}

void  babelServer::EventMgr::translateEvent(const Event *e, Packet& packet)
{
    packet.setIp(e->ipAddressUser);
    if (dynamic_cast<const ResponseEvent *>(e))
    {
        //Protocol<ReturnValue> packetStruct;
        Packet::Protocol<ReturnValue>::translateData(*static_cast<const ResponseEvent *>(e), packet);
    }
    else
    {
        Packet::Protocol<std::string>::translateData(*e, packet);
    }
}

void	babelServer::EventMgr::actionWithNoneArg(struct Event &, char *, unsigned int)
{
}

void		babelServer::EventMgr::actionWithAnArg(struct Event &event,
                                                   char *data, unsigned int size)
{
  std::string 	arg = "";
  unsigned int  i = 0;

  while (i < size && data[i])
    {
      arg += data[i];
      i += 1;
    }
  event.data.push_back(arg);
}

void	babelServer::EventMgr::actionWithTwoArgs(struct Event &event,
                                                 char *data, unsigned int size)
{
  std::string 	arg = "";
  unsigned int 	i = 0;

  while (i < size && data[i])
    {
      arg += data[i];
      i += 1;
    }
  event.data.push_back(arg);
  i += 1;
  arg.clear();
  while (i < size && data[i])
    {
      arg += data[i];
      i += 1;
    }
  if (arg.size() == 0)
      throw std::runtime_error("Bad number of arguments");
  event.data.push_back(arg);
}

/* to be tested implicitly */

babelServer::ResponseEvent::ResponseEvent(Event const &e) : Event(e)
{}

babelServer::ResponseEvent::ResponseEvent(Event const &e, ReturnValue r) : ResponseEvent(e)
{
    ret = r;
}