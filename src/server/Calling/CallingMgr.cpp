//
// Created by mymy on 02/10/17.
//

#include "server/Chief/ChiefLeader.hpp"
#include "server/Calling/CallingMgr.hpp"

babelServer::CallingMgr::CallingMgr(ChiefLeader *chief) :
ASystem(chief), _actionFunctions(), _calls()
{
  this->_actionFunctions.insert(std::make_pair(ActionType::STARTSESSION, std::bind(&CallingMgr::callingUser, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::STOPSESSION, std::bind(&CallingMgr::hangUp, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DISCONNECTION, std::bind(&CallingMgr::hangUp, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::ACCEPTSESSION, std::bind(&CallingMgr::acceptCall, this, std::placeholders::_1)));
  this->_actionFunctions.insert(std::make_pair(ActionType::DECLINESESSION, std::bind(&CallingMgr::refuseCall, this, std::placeholders::_1)));
}

void babelServer::CallingMgr::getEvent(struct Event const *event)
{
  if (!dynamic_cast<const ResponseEvent *>(event) && this->_actionFunctions.find(event->actionType) != this->_actionFunctions.end())
    {
      this->_actionFunctions[event->actionType](event);
    }
}

void 	babelServer::CallingMgr::callingUser(struct Event const *event)
{
  int 	idxCall;

  if ((idxCall = isCallExists(event->ipAddressUser)) >= 0)
    {
      if (event->ipAddressUser == this->_calls[idxCall]._ipCalling)
	{
	  ResponseEvent responseEvent(*event, ReturnValue::UNKNOWN_ERROR);

	  this->sendEvent(responseEvent);
	}
      else
	{
	  Event	newEvent;

	  newEvent.ipAddressUser = event->ipAddressUser;
	  newEvent.actionType = ActionType::DECLINESESSION;
	  this->sendEvent(newEvent);
	}
    }
  else
    {
      if (this->_chief->_databaseMgr.isUserConnected(event->ipAddressUser)
	      && this->_chief->_databaseMgr.isUserConnected(event->data[0]))
	{
	  std::string pseudo = this->_chief->_databaseMgr.getPseudoByIP(event->ipAddressUser);
	  if (this->_chief->_databaseMgr.isContact(event->data[0], pseudo))
	    {
	      ResponseEvent responseEvent(*event, ReturnValue::OK);
	      this->sendEvent(responseEvent);

	      Event	newEvent;

	      newEvent.ipAddressUser = this->_chief->_databaseMgr.getIpByPseudo(event->data[0]);
	      newEvent.actionType = ActionType::CALLING;
	      newEvent.data.push_back(pseudo);
	      newEvent.data.push_back(event->ipAddressUser.to_string());
            this->sendEvent(newEvent);

	      std::mutex pierie;

	      pierie.lock();
	      Call call;

	      call._ipCalled = newEvent.ipAddressUser;
	      call._ipCalling = event->ipAddressUser;

	      call.onGoing = false;

	      this->_calls.push_back(call);
	      pierie.unlock();

	      return;
	    }
	}
      ResponseEvent responseEvent(*event, ReturnValue::UNKNOWN_ERROR);

      this->sendEvent(responseEvent);
    }
}

void	babelServer::CallingMgr::hangUp(struct Event const *event)
{
  if (isInCommunication(event->ipAddressUser))
    {
      ResponseEvent responseEvent(*event, ReturnValue::OK);
      this->sendEvent(responseEvent);

      Event newEvent;
      newEvent.actionType = ActionType::HANGUP;
      newEvent.data.push_back(this->_chief->_databaseMgr.getPseudoByIP(event->ipAddressUser));
      newEvent.ipAddressUser = this->getOtherIp(event->ipAddressUser);
      this->sendEvent(newEvent);

      this->eraseCall(event->ipAddressUser);
    }
  else if (this->_chief->_databaseMgr.isUserConnected(event->ipAddressUser))
    {
      ResponseEvent responseEvent(*event, ReturnValue::UNKNOWN_ERROR);

      this->sendEvent(responseEvent);
    }

}

void	babelServer::CallingMgr::acceptCall(struct Event const *event)
{
  int 	idxCall;

  if (((idxCall = this->isCallExists(event->ipAddressUser)) >= 0)
    	&& this->_calls[idxCall]._ipCalled == event->ipAddressUser)
    {

      std::mutex lulie;
      lulie.lock();
      this->_calls[idxCall].onGoing = true;
      lulie.unlock();

      ResponseEvent responseEvent(*event, ReturnValue::OK);
      this->sendEvent(responseEvent);

      Event	newEvent;
      newEvent.ipAddressUser = this->_calls[idxCall]._ipCalling;
      newEvent.actionType = ActionType::CALLING_ACCEPTED;
      newEvent.data.push_back(this->_chief->_databaseMgr.getPseudoByIP(event->ipAddressUser));
      newEvent.data.push_back(this->_calls[idxCall]._ipCalled.to_string());
      this->sendEvent(newEvent);
    }
  else
    {
      ResponseEvent responseEvent(*event, ReturnValue::UNKNOWN_ERROR);

      this->sendEvent(responseEvent);
    }
}

void	babelServer::CallingMgr::refuseCall(struct Event const *event)
{
  int 	idxCall;

  if (((idxCall = this->isCallExists(event->ipAddressUser)) >= 0)
      && this->_calls[idxCall]._ipCalled == event->ipAddressUser)
    {
      this->eraseCall(event->ipAddressUser);

      ResponseEvent responseEvent(*event, ReturnValue::OK);
      this->sendEvent(responseEvent);

      Event	newEvent;

      newEvent.ipAddressUser = this->_calls[idxCall]._ipCalled;
      newEvent.actionType = ActionType::CALLING_DECLINED;
      newEvent.data.push_back(this->_chief->_databaseMgr.getPseudoByIP(event->ipAddressUser));
      this->sendEvent(newEvent);
    }
  else
    {
      ResponseEvent responseEvent(*event, ReturnValue::UNKNOWN_ERROR);

      this->sendEvent(responseEvent);
    }
}

bool	babelServer::CallingMgr::isInCommunication(boost::asio::ip::address const &ip)
{
  for (auto &call : this->_calls)
    {
      if ((call._ipCalled == ip || call._ipCalling == ip) && call.onGoing)
	return (true);
    }
  return (false);
}

int	babelServer::CallingMgr::isCallExists(boost::asio::ip::address const &ip)
{
  int 	idx = 0;

  for (auto &call : this->_calls)
    {
      if (call._ipCalled == ip || call._ipCalling == ip)
	return (idx);
      idx += 1;
    }
  return (-1);
}

void		babelServer::CallingMgr::eraseCall(boost::asio::ip::address const &ip)
{
  size_t 	idx = 0;

  for (auto &call : this->_calls)
    {
  	if (call._ipCalled == ip || call._ipCalling == ip)
	  break;
      idx += 1;
    }

  std::mutex nomie;
  nomie.lock();
  this->_calls.erase(this->_calls.begin() + idx);
  nomie.unlock();
}

boost::asio::ip::address const& babelServer::CallingMgr::getOtherIp(boost::asio::ip::address const &ip)
{
  for (auto &call : this->_calls)
    {
      if (call._ipCalled == ip)
	return (call._ipCalling);
      else if (call._ipCalling == ip)
	  return (call._ipCalled);
    }
  throw std::runtime_error("Error : A user is alone in his call.");
}