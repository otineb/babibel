//
// Created by mymy on 27/09/17.
//

#include <stdexcept>
#ifdef __unix__
#include <dirent.h>
#elif _WIN32
	#include <Windows.h>
	#include <iostream>
	#include <string>
#endif
#include <cstring>

#include "server/File/FileManager.hpp"

FileManager::FileManager(std::string const& file) : _fileName(file)
{}

#ifdef __unix__
void		FileManager::listFiles(std::string const& path, std::vector<std::string> &files)
{
  DIR 		*dpdf;
  struct dirent *epdf;

  dpdf = opendir(path.c_str());
  if (dpdf != nullptr)
    {
      while ((epdf = readdir(dpdf)) != nullptr)
	{
	  if (epdf->d_type == DT_DIR && strstr(epdf->d_name, "..") == nullptr && strstr(epdf->d_name,".") == nullptr)
	    {
	      listFiles(path + "/" + epdf->d_name + "/", files);
	    }
	  if (epdf->d_type == DT_REG)
	    {
	      files.push_back(path + epdf->d_name);
	    }
	}
    }
  closedir(dpdf);
}

#elif _WIN32
void 	FileManager::listFiles(std::string const& path, std::vector<std::string> &files)
{
	HANDLE _hfind;
	WIN32_FIND_DATA FindFileData;

	_hfind = FindFirstFile(path.c_str(), &FindFileData);
	if (_hfind != INVALID_HANDLE_VALUE)
	{
		while (FindNextFile(_hfind, &FindFileData))
		{
			if (FindFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY &&
			    strstr(FindFileData.cFileName, "..") == NULL &&
				strstr(FindFileData.cFileName, ".") == NULL)
			{
			    listFiles(path + "/" + FindFileData.cFileName + "/", files);
			}
			if (FindFileData.dwFileAttributes == FILE_ATTRIBUTE_NORMAL)
				files.push_back(path + FindFileData.cFileName);
		}
	}
	FindClose(_hfind);
}
#endif

std::string             FileManager::readFile() const
{
  std::ifstream         f(_fileName.c_str());
  std::string           buffer;

  if (!f.is_open())
    throw std::runtime_error("Couldn't open file " + _fileName);
  f.seekg(0, std::ios::end);
  buffer.resize(f.tellg());
  f.seekg(0);
  f.read((char *)buffer.data(), buffer.size());
  f.close();
  return (buffer);
}

std::string             FileManager::readFifo() const
{
  std::ifstream         f(_fileName.c_str());
  std::string           buffer;
  std::string           line;

  if (!f.is_open())
    throw std::runtime_error("Couldn't open file " + _fileName);
#ifdef __unix__
  while (std:: getline(f, line))
    buffer += line;
#elif _WIN32
  while (std::getline(f, line))
          buffer += line;
#endif
  return (buffer);
}

std::string             FileManager::readStdin()
{
  std::string           buffer;

#ifdef __unix__
  std::getline(std::cin, buffer);
#elif _WIN32
  std::getline(std::cin, buffer);
#endif
  if (std::cin.eof())
    throw std::runtime_error("");
  if (buffer.empty())
    return (" ");
  return (buffer);
}

void            FileManager::writeFile(std::string const& buffer) const
{
  std::ofstream f(_fileName.c_str());

  if (f.is_open())
    {
      f.write(buffer.data(), buffer.size());
      f.close();
    }
}

void            FileManager::openFile(std::string const& file)
{
  _fileName = file;
}


