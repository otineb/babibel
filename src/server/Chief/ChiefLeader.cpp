//
// Created by mymy on 29/09/17.
//

#include "server/Chief/ChiefLeader.hpp"

babelServer::ChiefLeader::ChiefLeader(NetworkManager &networkManager) :
_eventMgr(), _callingMgr(this), _networkMgr(networkManager), _databaseMgr(this)
{}

void        babelServer::ChiefLeader::translateActions(Packet &packet, const struct Event *event)
{
    this->_eventMgr.translateEvent(event, packet);
}

void 		babelServer::ChiefLeader::createActions(Packet packet)
{
  std::unique_ptr<Event> event(this->_eventMgr.translateInfo(packet));
  //std::cout << "event from -> "<< event->ipAddressUser.to_string() << std::endl;
  this->transmitEvent(*event);
}

void		babelServer::ChiefLeader::transmitEvent(Event const &event)
{
  //std::cout << "IN TRANSMIT EVENT" << std::endl;
  this->_databaseMgr.getEvent(&event);
  this->_networkMgr.getEvent(&event);
  this->_callingMgr.getEvent(&event);
}

void		babelServer::ChiefLeader::receiveEvent(Event const &event)
{
  this->transmitEvent(event);
}