//
// Created by benito on 28/09/17.
//

#include "server/Database/User.hpp"

User::User(std::string const &pseudo, std::string const &password) :
        pseudo_(pseudo), password_(password), ip_(), connected_(false), state_(User::State::DEFAULT) {}

boost::asio::ip::address const& User::getIp() const
{
  return (this->ip_);
}

std::string const& User::getPseudo() const
{
  return (this->pseudo_);
}

std::string const& User::getPassword() const
{
  return (this->password_);
}

void 	User::setPassword(std::string const &newPassword)
{
  this->password_ = newPassword;
}

void	User::setIpAddress(boost::asio::ip::address const &ip)
{
  this->ip_ = ip;
}

void	User::connect()
{
  this->connected_ = true;
}

void 	User::disconnect()
{
  this->connected_ = false;
}

bool	User::isConnected() const
{
  return (this->connected_);
}