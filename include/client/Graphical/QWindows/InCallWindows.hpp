#pragma once

# include <QtCore/QVariant>
# include <QtWidgets/QAction>
# include <QtWidgets/QApplication>
# include <QtWidgets/QButtonGroup>
# include <QtWidgets/QHeaderView>
# include <QtWidgets/QLabel>
# include <QtWidgets/QPushButton>
# include <QtWidgets/QVBoxLayout>
# include <QtWidgets/QWidget>

# include <iostream>

# include "include/client/Interfaces/IWindows.hpp"

	namespace Graphical
	{
		class InCallWindows : public QWidget, public IWindows
		{
			Q_OBJECT

		public:
			InCallWindows(QWidget & Windows);
			virtual ~InCallWindows();


		//IWindows
		
		public:

			virtual void				ShowWindows();
			virtual void				HideWindows();
			virtual WindowType			Type() const;
			virtual bool				TreatEvent(Event::Event const &event);

		//UI

		private:
			QVBoxLayout					*verticalLayout;
			QLabel							*Label_NameOfCaller;
			QLabel							*Label_CallLength;
			QPushButton						*Button_HangUp;


			void						setupConnectSlot();
			void						setupUi();
			void						retranslateUi();
			void						updateValue();

		public:
			

		public slots:

			void						Pressed_HangUp();
		};
	}