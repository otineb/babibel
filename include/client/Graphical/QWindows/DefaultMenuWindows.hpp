#pragma once

# include <QtCore/QVariant>
# include <QtWidgets/QAction>
# include <QtWidgets/QApplication>
# include <QtWidgets/QButtonGroup>
# include <QtWidgets/QHBoxLayout>
# include <QtWidgets/QHeaderView>
# include <QtWidgets/QLabel>
# include <QtWidgets/QListWidget>
# include <QtWidgets/QPushButton>
# include <QtWidgets/QVBoxLayout>
# include <QtWidgets/QWidget>

# include <iostream>

# include "include/client/Interfaces/IWindows.hpp"

	namespace Graphical
	{
		class DefaultMenuWindows : public QWidget, public IWindows
		{
			Q_OBJECT

		public:
			DefaultMenuWindows(QWidget & Windows);
			virtual ~DefaultMenuWindows();


		//IWindows
	
		public:

			virtual void				ShowWindows();
			virtual void				HideWindows();
			virtual WindowType			Type() const;
			virtual bool				TreatEvent(Event::Event const &event);

		//UI

		private:
			QVBoxLayout		*verticalLayout;
			QLabel				*Label_Name;
			QListWidget			*List_Contact;
			QHBoxLayout			*Layout_Buttons;
			QPushButton				*Button_UnFriend;
			QPushButton				*Button_Call;
			QPushButton			*Button_AddFriend;

			void			setupConnectSlot();
			void			setupUi();
			void			retranslateUi();
			void			updateValue();

		public:
			

		public slots:

			void			Pressed_Call();
			void			Pressed_UnFriend();
			void			Pressed_AddFriend();
		};
	}