#pragma once

# include <QMainWindow>
# include <QPushButton>
# include <iostream>

# include "include/client/Interfaces/IWindows.hpp"

	namespace Graphical
	{
		class MainWindows : public QMainWindow, public IWindows
		{
			Q_OBJECT
		private:
			int		_Width;
			int		_Height;

		public:
			MainWindows();
			MainWindows(int Width, int Height);
			~MainWindows();

		private:
			void InitQtObject();

		protected:
			bool						event(QEvent *event);

		public:
			virtual void				ShowWindows();
			virtual void				HideWindows();
			virtual	WindowType			Type() const;
			virtual bool				TreatEvent(Event::Event const &);
		};
	}