#pragma once

# include <QtCore/QVariant>
# include <QtWidgets/QAction>
# include <QtWidgets/QApplication>
# include <QtWidgets/QButtonGroup>
# include <QtWidgets/QFrame>
# include <QtWidgets/QHBoxLayout>
# include <QtWidgets/QHeaderView>
# include <QtWidgets/QLabel>
# include <QtWidgets/QLineEdit>
# include <QtWidgets/QPushButton>
# include <QtWidgets/QVBoxLayout>
# include <QtWidgets/QWidget>

# include <iostream>

# include "include/client/Interfaces/IWindows.hpp"

	namespace Graphical
	{
		class AddAFriendWindows : public QWidget, public IWindows
		{
			Q_OBJECT

		public:
			AddAFriendWindows(QWidget & Windows);
			virtual ~AddAFriendWindows();


		//IWindows
	
		public:

			virtual void				ShowWindows();
			virtual void				HideWindows();
			virtual WindowType			Type() const;
			virtual bool				TreatEvent(Event::Event const &event);

		//UI

		private:
			QVBoxLayout					*verticalLayout;
			QFrame						*Frame_1;
			QHBoxLayout					*horizontalLayout;
			QLabel							*Label_Name;
			QLineEdit						*Edit_Name;
			QLabel						*Label_Error;
			QFrame						*Frame_2;
			QHBoxLayout					*horizontalLayout_2;
			QPushButton						*Button_Cancel;
			QPushButton						*Button_Add;

			void						setupConnectSlot();
			void						setupUi();
			void						retranslateUi();
			void						updateValue();

		public:
			

		public slots:

			void						Pressed_Add();
			void						Pressed_Cancel();
		};
	}