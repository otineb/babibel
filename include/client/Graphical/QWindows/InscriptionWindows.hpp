#pragma once

# include <QtCore/QVariant>
# include <QtWidgets/QAction>
# include <QtWidgets/QApplication>
# include <QtWidgets/QButtonGroup>
# include <QtWidgets/QHeaderView>
# include <QtWidgets/QLabel>
# include <QtWidgets/QLineEdit>
# include <QtWidgets/QPushButton>
# include <QtWidgets/QVBoxLayout>
# include <QtWidgets/QWidget>

# include <iostream>

# include "include/client/Interfaces/IWindows.hpp"

	namespace Graphical
	{
		class InscriptionWindows: public QWidget, public IWindows
		{
			Q_OBJECT

		public:
			InscriptionWindows(QWidget & Windows);
			virtual ~InscriptionWindows();
			
		//IWindows

		public:

			virtual void				ShowWindows();
			virtual void				HideWindows();
			virtual WindowType			Type() const;
			virtual bool				TreatEvent(Event::Event const &event);

		//UI

		private:
			QVBoxLayout			*verticalLayout;
			QLabel					*Label_Title;
			
			QLabel					*Label_UserName;
			QLineEdit				*Input_Username;
			
			QLabel					*Label_Password;
			QLineEdit				*Input_Password;
			QLabel					*Label_Error;

			QHBoxLayout				*Box_Connect;
			QPushButton					*Button_Connect;
			
			QHBoxLayout				*Box_CreateAccount;
			QPushButton					*Button_CreateAccount;

			void				setupConnectSlot();
			void				setupUi();
			void				retranslateUi();

		private:
			std::vector<std::string>		GetConnectionData() const;

		public slots:

			void				Pressed_Connect();
			void				Pressed_CreateAccount();
		};
	}