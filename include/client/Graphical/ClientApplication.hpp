#pragma once

# include <QApplication>

# include <iostream>

	namespace Graphical
	{
		class ClientApplication: public QApplication
		{
		public:
			ClientApplication(int argc, char **argv);
			~ClientApplication();

		protected:
			bool event(QEvent *event);
		};
	}