#pragma once

# include <QWidget>
# include <QtUiTools>

# include <memory>

# include "include/client/Interfaces/IEventUser.hpp"
# include "include/client/Interfaces/IWindows.hpp"

# include "ClientApplication.hpp"
# include "include/client/Graphical/QWindows/MainWindows.hpp"

	namespace Graphical
	{
		class GraphicalManager: public IEventUser
		{
		private:
			static GraphicalManager		*_instance;
		public:
			static GraphicalManager		*GetSingleton();

		private:
			QApplication								_App;
			MainWindows									_MainWindows;
			std::vector<std::shared_ptr<IWindows> >		_windows;

		public:
			GraphicalManager(int argc, char **argv);
			virtual ~GraphicalManager();
			
			void						InitWindows();

		public:
			int							Execute();

		public:
			virtual bool				TreatEvent(Event::Event const &event);
		
			void						ShowSubView(WindowType const & type);
			void						HideSubView(WindowType const & type);
			std::shared_ptr<IWindows>	GetSubView(WindowType const & type) const;
		};
	}
