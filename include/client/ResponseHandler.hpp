//
// Created by daze on 10/5/17.
//

#ifndef BABEL_RESPONSEHANDLER_HPP
#define BABEL_RESPONSEHANDLER_HPP

#include <unordered_map>
#include <functional>
#include <QByteArray>
#include "ActionType.hpp"
#include "Interfaces/IEventUser.hpp"

class       ResponseHandler
{
private:
    Event::EventType calling(QByteArray const &);
    Event::EventType hangUp(QByteArray const &);
    Event::EventType timeOut(QByteArray const &);
    Event::EventType connection(QByteArray const &);
    Event::EventType registration(QByteArray const &);
    Event::EventType disconnection(QByteArray const &);
    Event::EventType addContact(QByteArray const &);
    Event::EventType acceptSession(QByteArray const &);
    Event::EventType callAccepted(QByteArray const &);
    Event::EventType updateContactList(QByteArray const &);

    std::unordered_map<ActionType, std::function<Event::EventType(QByteArray const &)>> _map =
            {
                    {ActionType::CALLING, [this](QByteArray const &buffer){return (this->calling(buffer));}},
                    {ActionType::HANGUP, [this](QByteArray const &buffer){return (this->hangUp(buffer));}},
                    {ActionType::TIMEOUT, [this](QByteArray const &buffer){return (this->timeOut(buffer));}},
                    {ActionType::CONNECTION, [this](QByteArray const &buffer){return (this->connection(buffer));}},
                    {ActionType::REGISTER, [this](QByteArray const &buffer){return (this->registration(buffer));}},
                    {ActionType::DISCONNECTION, [this](QByteArray const &buffer){return (this->disconnection(buffer));}},
                    {ActionType::ADDCONTACT, [this](QByteArray const &buffer){return (this->addContact(buffer));}},
                    {ActionType::CALLING_ACCEPTED, [this](QByteArray const &buffer){return (this->callAccepted(buffer));}},
                    {ActionType::HANGUP, [this](QByteArray const &buffer){return (this->hangUp(buffer));}},
                    {ActionType::UPDATECONTACTLIST, [this](QByteArray const &buffer){return (this->updateContactList(buffer));}},
                    {ActionType::ACCEPTSESSION, [this](QByteArray const &buffer){return (this->acceptSession(buffer));}}
            };

public:
    std::unordered_map<ActionType, std::function<Event::EventType(QByteArray const&)>> const &getMap() const;


    ResponseHandler() = default;
    ~ResponseHandler() = default;
};

#endif //BABEL_RESPONSEHANDLER_HPP
