//
// Utils.hpp for Utils.hpp in /home/daze/rendu/cpp/babibel/build
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Mon Oct  2 18:59:42 2017 DaZe
// Last update Mon Oct  2 19:08:21 2017 DaZe
//

#pragma once

#include <QObject>
#include <cstring>
#include <string>
#include <iostream>
#include "ActionType.hpp"

class	Utils
{
public:
  static int QByteArrayToInt(QByteArray const &array)
  {
    int a;
    std::memcpy(&a, array, sizeof(int));
    return (a);
  }

   static ActionType QByteArrayToUS(QByteArray const &array)
  {
    ActionType a;
    std::memcpy(&a, array, sizeof(unsigned short));
    return (a);
  }

    static std::vector<std::string> QByteArrayToStringVec(int size, QByteArray const &array)
    {
        std::vector<std::string>    vec;
        int                         idx = 0;

        while (idx < size)
        {
            std::string buff;
            while (idx < size && array[idx] != 0)
            {
                buff.append(1, array[idx]);
                idx++;
            }
            vec.push_back(buff);
            idx++;
        }
        return(vec);
    }

};
