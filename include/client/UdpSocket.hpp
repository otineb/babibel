//
// UdpSocket.hpp for UdpSocket.hpp in /home/daze/rendu/c++/babibel/QT_network_test/build
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Thu Sep 28 16:12:49 2017 DaZe
// Last update Sat Oct  7 17:43:17 2017 Mymy Aragon
//

#pragma once

#include <QtNetwork/QUdpSocket>
#include <QObject>
#include <QUdpSocket>
#include <include/client/ActionType.hpp>
#include "client/Audio/AudioManager.hpp"

class UdpSocket : public QObject
{
  Q_OBJECT
  
private:
  QHostAddress		_destAddress;
  quint16		_portRead;
  quint16		_portWrite;
  AudioManager      	_audioMgr;
  QUdpSocket		_socket;

public:
  UdpSocket(std::string const &);
  virtual ~UdpSocket();

  void			run();
  void			setReceiverAddress(std::string const &str);
  QHostAddress		getReceiverAddress();
  

						  
public slots:
  virtual void		readPendingDatagrams();					  
  virtual void	        sendDatagram();
};
