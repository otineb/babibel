//
// ActionType.hpp for ActionType.hpp in /home/daze/rendu/c++/babibel/build
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Fri Sep 29 18:04:13 2017 DaZe
// Last update Sun Oct  1 15:24:38 2017 Myriam
//

#pragma once

enum class ActionType : unsigned short
  {
    UNKNOWN = 0,
    CALLING = 51,
    HANGUP = 52,
    RECEIVE_INVIT = 53,
    TIMEOUT = 54,
    CALLING_ACCEPTED = 55,
    UPDATECONTACTLIST = 57,
    REGISTER = 100,
    SUPPRESSION = 110,
    CONNECTION = 120,
    DISCONNECTION = 130,
    GETCONTACTLIST = 140,
    GETINVITATIONLIST = 150,
    GETCONTACT = 160,
    ADDCONTACT = 170,
    DELETECONTACT = 180,
    ACCEPTCONTACT = 190,
    DECLINECONTACT = 200,
    CHANGEPASSWORD = 210,
    STARTSESSION = 220,
    STOPSESSION = 230,
    ACCEPTSESSION = 240,
    DECLINESESSION = 250,
    ADDTOSESSION = 260,
    KICKFROMSESSION = 270
};

struct Packet
{
  int	size;
  unsigned short type;
  const char *data;
};
