//
// NetworkMgr.hpp for NetworkMgr.hpp in /home/daze/rendu/c++/babibel
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Fri Sep 29 16:37:10 2017 DaZe
// Last update Wed Oct  4 16:15:32 2017 Myriam
//

#pragma once

#include "TcpSocket.hpp"
#include "UdpSocket.hpp"

class		NetworkMgr
{
  TcpSocket     *_tcpSocket;

public:
    UdpSocket     *_udpSocket;
    TcpSocket	*getTcpSocket() const;

  NetworkMgr();
  ~NetworkMgr() = default;

  void		connectUdp(std::string const &);
    void    disconnectUdp();
};
