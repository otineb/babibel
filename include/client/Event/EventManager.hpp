#pragma once

# include <vector>
# include <mutex>

# include "Event.hpp"

	namespace Event
	{
		class EventManager
		{
		private:
			static EventManager			*_instance;
		public:
			static EventManager			*GetSingleton();

		private:

			std::vector<Event>			_eventList;
			std::mutex					_eventListMtx;

		public:
			EventManager();
			~EventManager();

		public:
			std::vector<Event>			GetEventList();
			bool						IsEmpty();

			void						AddEvent(EventType type);
			void						AddEvent(EventType type, std::vector<std::string> const &data);
			static void					Singleton_AddEvent(EventType type);
			static void					Singleton_AddEvent(EventType type, std::vector<std::string> const &data);
		};
	}
