#pragma once

# include <string>
# include <vector>

	namespace Event
	{
		enum class EventType : unsigned short
		{
			// Inscription Button

			Connect			= 120,
			CreateAccount	= 100,

			Disconnection	= 130,

			// Default Button

			Calling			= 220,
			AddAFriend		= 0x003,
			UnFriend		= 180,

			// Called Button

			CallAccepted	= 240,
			CallRefused		= 250,

			// Call Button

			HangUp			= 230,

			// AddFriend Button

			TryAddFriend	= 170,
			CancelAddFri	= 0x009,

			// NetworkMgr Event

			Timeout			= 54,

			ConnectionAcc	= 0x100,		// Connection Accepted and send the name in the data => Go from the inscription Page to the default one
			ConnectionRef	= 0x101,		// Connection Refused and Get error in Data => Stay on page
			
			AccountAccept	= 0x102,		// Account Accepted and send the name in the data => Go from the inscription Page to the default one
			AccountRefuse	= 0x103,		// Account Refused and Get error in Data => Stay on page

			ReceiveCall		= 51,		// Receive Call => Show ReceiveCall page

			CallBegin		= 55,		// Call Begin => Show InCall page // Hide ReceiveCall page
			CallEnd			= 52,				// Call End => Hide InCall page
	
			FriendAdded		= 0x107,		// Friend Added with success => Hide AddFriend page
			FriendAddErr	= 0x108,			// FriendAdd Failed with error code in data => Stay on page

			UpdateContactList = 0x500
		};

		class Event
		{
		private:
			EventType					_type;
			std::vector<std::string>	_data;

		public:
			Event(EventType type);
			Event(EventType type, std::vector<std::string> const &data);
			~Event();

		public:
			std::vector<std::string> const &	Data() const;
			EventType const &					Type() const;
		};
	}