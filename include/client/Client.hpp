//
// Client.hpp for Client.hpp in /home/daze/rendu/c++/babibel/QT_network_test
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Thu Sep 28 17:45:43 2017 DaZe
// Last update Sat Oct  7 14:29:16 2017 daze
//

#pragma once

#include <unordered_map>
#include <thread>
#include <include/client/Audio/AudioManager.hpp>

#include "include/client/Interfaces/IEventUser.hpp"
#include "include/client/Event/EventManager.hpp"
#include "include/client/Graphical/GraphicalManager.hpp"

#include "NetworkMgr.hpp"

    class Client : public IEventUser
    {
        NetworkMgr                  *_netMgr;
        static Client               *_instance;
        std::string                 _clientName;
        std::vector<std::string>    _friendList;
        std::string                 _callerName;
        Graphical::GraphicalManager _graphMgr;
        Event::EventManager         _eventMgr;
        std::vector<IEventUser *>   _eventUsers;
        std::string                 _ipOtherClient;
      
      
        enum State :    int
        {
            Start     = 0x00,
            End       = 0x01,

            Default   = 0x10,
	    CallBegin = 0x13,
            Calling   = 0x11,
            EndCall   = 0x12,
        };
        State                       _state;
      char			    **av;

    public:

        void registration(std::vector<std::string> const&, struct Packet &packet);

        void deletion(std::vector<std::string> const&, struct Packet &packet);

        void connection(std::vector<std::string> const&, struct Packet &packet);

        void disconnection(std::vector<std::string> const&, struct Packet &packet);

        void getContactList(std::vector<std::string> const&, struct Packet &packet);

        void getInvitationList(std::vector<std::string> const&, struct Packet &packet);

        void getContact(std::vector<std::string> const&, struct Packet &packet);

        void addContact(std::vector<std::string> const&, struct Packet &packet);

        void acceptContact(std::vector<std::string> const&, struct Packet &packet);

        void declineContact(std::vector<std::string> const&, struct Packet &packet);

        void delContact(std::vector<std::string> const&, struct Packet &packet);

        void changePasswd(std::vector<std::string> const&, struct Packet &packet);

        void startSession(std::vector<std::string> const&, struct Packet &packet);

        void stopSession(std::vector<std::string> const&, struct Packet &packet);

        void acceptSession(std::vector<std::string> const&, struct Packet &packet);

        void declineSession(std::vector<std::string> const&, struct Packet &packet);

        void addToSession(std::vector<std::string> const&, struct Packet &packet);

        void kickFromSession(std::vector<std::string> const&, struct Packet &packet);

    private:

        std::unordered_map<ActionType, std::function<void(std::vector<std::string> const&,
                                                          struct Packet &packet)>> _actions =
                {
                        {ActionType::REGISTER,          [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->registration(args, packet);
                        }},
                        {ActionType::SUPPRESSION,            [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->deletion(args, packet);
                        }},
                        {ActionType::CONNECTION,        [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->connection(args, packet);
                        }},
                        {ActionType::DISCONNECTION,     [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->disconnection(args, packet);
                        }},
                        {ActionType::GETCONTACTLIST,    [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->getContactList(args, packet);
                        }},
                        {ActionType::GETINVITATIONLIST, [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->getInvitationList(args, packet);
                        }},
                        {ActionType::GETCONTACT,        [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->getInvitationList(args, packet);
                        }},
                        {ActionType::ADDCONTACT,        [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->addContact(args, packet);
                        }},
                        {ActionType::DELETECONTACT,     [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->delContact(args, packet);
                        }},
                        {ActionType::ACCEPTCONTACT,     [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->acceptContact(args, packet);
                        }},
                        {ActionType::DECLINECONTACT,    [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->declineContact(args, packet);
                        }},
                        {ActionType::CHANGEPASSWORD,    [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->changePasswd(args, packet);
                        }},
                        {ActionType::STARTSESSION,      [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->startSession(args, packet);
                        }},
                        {ActionType::STOPSESSION,       [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->stopSession(args, packet);
                        }},
                        {ActionType::ACCEPTSESSION,     [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->acceptSession(args, packet);
                        }},
                        {ActionType::DECLINESESSION,    [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->declineSession(args, packet);
                        }},
                        {ActionType::ADDTOSESSION,      [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->addToSession(args, packet);
                        }},
                        {ActionType::KICKFROMSESSION,   [this](std::vector<std::string> const& args,
                                                               struct Packet &packet) {
                            this->kickFromSession(args, packet);
                        }},
                };

    public:

        void commandHandler(Event::EventType , std::vector<std::string> const&);

        NetworkMgr *getNetMgr() const;

        virtual bool						TreatEvent(Event::Event const &event);
        int                                 thread_Graphical();
        int                                 thread_Event();
        int                                 thread_Network();
        int                                 Execute();

        static Client                       *GetSingleton();
		std::string const &					GetCallerName() const;
		void								SetCallerName(std::string const &);
		std::string const &					GetClientName() const;
		void								SetClientName(std::string const &);
		std::vector<std::string> const &	GetClientFriendList() const;
		void								SetClientFriendList(std::vector<std::string> const &);

        Client() = default;
        Client(int, char **);

        ~Client();
    };
