//
// TcpSocket.hpp for TcpSocket.hpp in /home/daze/QT_network_test
// 
// Made by DaZe
// Login   <thomas.vigier@epitech.eu>
// 
// Started on  Tue Sep 26 15:44:39 2017 DaZe
// Last update Sun Oct  1 11:40:33 2017 DaZe
//

#pragma once

#include <QtNetwork/QTcpSocket>
#include <iostream>
#include "ResponseHandler.hpp"

class TcpSocket : public QObject, public IEventUser
{
    Q_OBJECT

private:
    std::string             _ip;
    int                     _port;
    QTcpSocket*	            _socket;
    QByteArray              _buffer;
    int                     _bufferSize;
    ResponseHandler         _handler;
public:
    virtual void            doConnect();

    virtual void            write(const struct Packet&);

    QTcpSocket*	            getSocket() const;
    QByteArray const &      getBuffer() const;
    void                    setBuffer(QByteArray const &);
    void			        setIp(std::string const &);
    void			        setPort(int);
    const std::string &	    getIp() const;
    int			            getPort() const;
    virtual bool            TreatEvent(Event::Event const &);
    void                    dataToEvent(int,
                                        ActionType,
                                        QByteArray const &);

    TcpSocket();
    TcpSocket(std::string);
    virtual ~TcpSocket() = default;

    signals:

    public slots:

    virtual void            connected();
    virtual void            disconnected();
    virtual void            bytesWritten(qint64);
    virtual void            readyRead();
};
