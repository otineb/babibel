//
// Created by pashervz on 03/10/17.
//

#ifndef BABEL_ERROROPUS_HPP
#define BABEL_ERROROPUS_HPP

#include <exception>
#include <string>

class ErrorOpus : public std::exception
{
    std::string     _errorMessage;

public:

    ErrorOpus(std::string const& errorMessage) throw();
    ErrorOpus(ErrorOpus const & other);
    virtual ~ErrorOpus() throw();

    ErrorOpus& operator=(ErrorOpus const &);

    virtual const char *what() const throw();
};

#endif //BABEL_ERROROPUS_HPP
