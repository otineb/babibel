//
// Created by pashervz on 29/09/17.
//

#ifndef BABEL_PARAMS_HPP
#define BABEL_PARAMS_HPP

#include <portaudio.h>
#include "DeviceType.hpp"

# define MONO 1
# define STEREO 2

class   Params
{
public:
    PaStreamParameters  _params;

    explicit Params(DeviceType);
    ~Params() = default;
    void            InitInputParamsDefault();
    void            InitOutputParamsDefault();
};

#endif //BABEL_PARAMS_HPP
