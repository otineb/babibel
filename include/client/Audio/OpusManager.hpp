//
// Created by pashervz on 03/10/17.
//

#ifndef BABEL_OPUSMANAGER_HPP
#define BABEL_OPUSMANAGER_HPP

#include <opus.h>

class                           OpusManager
{
private:

    OpusEncoder                 *_encoder;
    OpusDecoder                 *_decoder;
    int                         _nbEncodedBytes;
    std::vector<short>          _recData;
    std::vector<unsigned char>  _toSendData;
    std::vector<unsigned char>	_receivedData;
    std::vector<short>          _decodData;

public:

    OpusManager() = default;
    ~OpusManager();

    int                                 getNbEncodedBytes() const
    {return this->_nbEncodedBytes;}

    void                                createEncoder(int, int);
    void                                createDecoder(int, int);

    void                                doEncode(int);
    void                                doDecode(int, int);
    void                                setRecData(std::vector<short> const &);
    void                                setEncodData(std::vector<unsigned char> const &);
    void				setReceivedData(std::vector<unsigned char> const &);
    void				setToSendData(std::vector<unsigned char> const &);
    std::vector<short> const &          getRecData() const { return _recData; }
    std::vector<unsigned char> const &  getToSendData() const { return _toSendData; }
    std::vector<short> const &          getDecodData() const { return _decodData; }
    std::vector<unsigned char> const &  getReceivedData() const { return _receivedData; }
};


#endif //BABEL_OPUSMANAGER_HPP
