//
// Created by pashervz on 29/09/17.
//

#ifndef BABEL_STREAM_HPP
#define BABEL_STREAM_HPP

#include <portaudio.h>
#include <queue>
#include <vector>
#include "Params.hpp"
#include "DeviceType.hpp"
#include "OpusManager.hpp"

class Stream
{
private:
    PaStream                                    *_stream;
    Params                                      _outputParams;
    Params                                      _inputParams;
    static const int                            RATE;
    static const unsigned long                  OPTIFRAMES;
    OpusManager                                 _compressor;
    std::queue<std::pair<int, std::vector<unsigned char>>>      _recQueue;
    std::queue<std::pair<int, std::vector<unsigned char>>>      _playQueue;

public:

    Stream();
    ~Stream() = default;

    static int                                  callBack(const void *,void *,
                                                         unsigned long, const PaStreamCallbackTimeInfo*,
                                                         PaStreamCallbackFlags, void *);
    void                                        openStream();
    void                                        closeStream();
    void                                        startStream();
    std::pair<int, std::vector<unsigned char>>                  sendRecData();
    void                                        putPlayData(int, unsigned char *);
};



#endif //BABEL_STREAM_HPP
