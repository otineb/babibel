//
// Created by pashervz on 28/09/17.
//

#ifndef BABEL_ERRORAUDIO_HPP
#define BABEL_ERRORAUDIO_HPP

# define ERROR 1

#include <exception>
#include <string>

class ErrorAudio    : public std::exception
{
    std::string     _errorMessage;
public:
    ErrorAudio(std::string const& errorMessage) throw();
    ErrorAudio(ErrorAudio const & other);
    virtual ~ErrorAudio() throw();

    ErrorAudio& operator=(ErrorAudio const &);

    virtual const char *what() const throw();
};

#endif //BABEL_ERRORAUDIO_HPP
