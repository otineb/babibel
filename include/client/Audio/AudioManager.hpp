//
// Created by pashervz on 28/09/17.
//

#ifndef BABEL_AUDIOMANAGER_HPP
#define BABEL_AUDIOMANAGER_HPP

#include "Stream.hpp"
#include "OpusManager.hpp"

class                           AudioManager
{
private:
    Stream                      _stream;

public:
    AudioManager();
    ~AudioManager();

    void                        init();
    void                        launch();
    void                        stop();
    std::pair<int, unsigned char *> getRecDataToSend();
    void                        pushData(unsigned char*);
    void                        Terminate();
};

#endif //BABEL_AUDIOMANAGER_HPP
