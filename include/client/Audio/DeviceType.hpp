//
// Created by pashervz on 02/10/17.
//

#ifndef BABEL_DEVICETYPE_HPP
#define BABEL_DEVICETYPE_HPP

enum    DeviceType
{
    AUDIO_INPUT,
    AUDIO_OUTPUT
};

#endif //BABEL_DEVICETYPE_HPP
