#pragma once

#include "IEventUser.hpp"

	namespace Graphical
	{
		enum WindowType : int
		{
			MainWindow		= 0x00,
			Inscription		= 0x01,
			DefaultMenu		= 0x02,
			Calling			= 0x03,
			InCall			= 0x04,
			AddFriend		= 0x05
		};

		class IWindows : public IEventUser
		{
		protected:
			bool						_showed;
		public:
			bool						Showed() const { return (_showed); };

		public:
			virtual ~IWindows() {};

			virtual void				ShowWindows() = 0;
			virtual void				HideWindows() = 0;
			virtual WindowType			Type() const = 0;
		};
	}