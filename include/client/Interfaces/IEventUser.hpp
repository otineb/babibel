#pragma once

#include "include/client/Event/Event.hpp"

	class IEventUser
	{
	public:
		virtual ~IEventUser() {}

		virtual bool		TreatEvent(Event::Event const &event) = 0;
	};