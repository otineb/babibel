//
// Created by mymy on 02/10/17.
//

#ifndef BABEL_CALLINGMGR_HPP
#define BABEL_CALLINGMGR_HPP

#include <boost/asio.hpp>
#include <mutex>
#include "server/ASystem.hpp"

namespace babelServer
{
  struct Call
  {
    boost::asio::ip::address  	_ipCalling;
    boost::asio::ip::address	_ipCalled;
    bool			onGoing;
  };

  class CallingMgr : public ASystem
  {
    std::unordered_map<ActionType, std::function<void(struct Event const *)>> _actionFunctions;
    std::vector<struct Call>	_calls;

   public:
    CallingMgr(ChiefLeader *);
    ~CallingMgr() = default;

    void 	getEvent(struct Event const *);

   private:
    void	callingUser(struct Event const *);
    void	acceptCall(struct Event const *);
    void 	refuseCall(struct Event const *);
    void	hangUp(struct Event const *);

    bool	isInCommunication(boost::asio::ip::address const &);
    int		isCallExists(boost::asio::ip::address const &);
    void	eraseCall(boost::asio::ip::address const &);

    boost::asio::ip::address const &getOtherIp(boost::asio::ip::address const &);
  };
}



#endif //BABEL_CALLINGMGR_HPP
