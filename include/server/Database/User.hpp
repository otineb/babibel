//
// Created by benito on 28/09/17.
//

#ifndef BABIBEL_USER_HPP
#define BABIBEL_USER_HPP

#include <iostream>
#include <boost/asio.hpp>

class   User
{
    enum class State
    {
        DEFAULT,
        CALLING,
        RECEIVING, //  external user trying to call
        TRYING // the user is trying
    };
 private:
  const std::string             pseudo_;
  std::string			password_;
  boost::asio::ip::address  	ip_;
  bool				connected_;
  State                         state_;
 public:
    User(std::string const&, std::string const&);
    ~User() = default;

  boost::asio::ip::address const &getIp() const;

  std::string const &getPseudo() const;
  std::string const &getPassword() const;
  bool	isConnected() const;

  void 	setPassword(std::string const &);
  void	setIpAddress(boost::asio::ip::address const &);
  void 	connect();
  void	disconnect();
};

#endif //BABIBEL_USER_HPP
