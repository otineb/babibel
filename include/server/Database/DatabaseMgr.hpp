//
// Created by mymy on 27/09/17.
//

#ifndef PROJECT_DATABASEMGR_HPP
#define PROJECT_DATABASEMGR_HPP

#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>
#include <mutex>
#include "server/ASystem.hpp"
#include "server/Database/User.hpp"
#include "server/Event/ActionType.hpp"

namespace babelServer {

    class DatabaseMgr : public ASystem {
    private:
        std::unordered_map<babelServer::ActionType, std::function<void(struct Event const *)>> _actionFunctions;

        std::vector<User> _users;
        std::unordered_map<std::string, std::vector<std::string>> _contactLists;
        boost::property_tree::ptree _root;
        boost::property_tree::ptree _contactRoot;

    public:
        DatabaseMgr(ChiefLeader *);

        ~DatabaseMgr() = default;

        void getEvent(struct Event const *);

      bool isUserConnected(boost::asio::ip::address const &) const;
      bool isUserConnected(std::string const &) const;
      std::string const &getPseudoByIP(boost::asio::ip::address const &)const;
      boost::asio::ip::address const &getIpByPseudo(std::string const &) const;
      bool isContact(std::string const &, std::string const &);


     private:
        void initActionFunction();

        void addContactList(std::string const &);

        void rewriteContactsFromUser(std::string const &);

        void registerUser(struct Event const *);

        void addFriend(struct Event const *);

        void deleteFriend(struct Event const *);

        void connect(struct Event const *);
        void disconnect(struct Event const *);


        bool isUserRegistered(std::string const &) const;

        int findUserWithIp(boost::asio::ip::address const &) const;
      int findUserWithName(std::string const &) const;


      void sendContactList(User const &);

    };
}
#endif //PROJECT_DATABASEMGR_HPP
