//
// Created by mymy on 29/09/17.
//

#ifndef BABIBEL_CHIEFLEADER_HPP
#define BABIBEL_CHIEFLEADER_HPP

#include <memory>
#include "server/Event/EventMgr.hpp"
#include "server/Network/NetworkManager.hpp"
#include "server/Database/DatabaseMgr.hpp"
#include "server/Database/User.hpp"
#include "server/Calling/CallingMgr.hpp"

namespace babelServer {
    class ChiefLeader {
      EventMgr 		_eventMgr;
      CallingMgr	_callingMgr;
      NetworkManager 	&_networkMgr;

    public:
        DatabaseMgr 	_databaseMgr;

        ChiefLeader(NetworkManager &);

        ~ChiefLeader() = default;

        void translateActions(Packet&, const struct Event *);

        void createActions(Packet packet);

        void receiveEvent(Event const &);

        void transmitEvent(Event const &);
    };
}


#endif //BABIBEL_CHIEFLEADER_HPP
