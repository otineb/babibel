//
// Created by benito on 02/10/17.
//

#ifndef BABEL_EVENT_HPP
#define BABEL_EVENT_HPP

#include "ActionType.hpp"
#include "boost/asio.hpp"
#include "ReturnValue.hpp"

namespace babelServer {
    struct Event {
        ActionType actionType;
        boost::asio::ip::address ipAddressUser;
        std::vector<std::string> data;

        virtual ~Event() {};
    };

    typedef struct Event Event;
    typedef struct ResponseEvent : public Event {
        ReturnValue ret;

        ResponseEvent() = default;

        ResponseEvent(Event const &); /* hum */
        ResponseEvent(Event const &, ReturnValue);

        virtual ~ResponseEvent() {};
    } ResponseEvent;
}
#endif //BABEL_EVENT_HPP
