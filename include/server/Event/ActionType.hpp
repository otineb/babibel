//
// Created by mymy on 28/09/17.
//

#ifndef BABIBEL_ACTIONTYPE_HPP
#define BABIBEL_ACTIONTYPE_HPP

namespace babelServer {

    enum class ActionType : unsigned short {
        UNKNOWN = 0,
        CALLING = 51,
        HANGUP = 52,
        RECEIVE_INVIT = 53,
        TIMEOUT = 54,
      	CALLING_ACCEPTED = 55,
      	CALLING_DECLINED = 56,
      	UPDATE_CONTACT_LIST = 57,
        REGISTER = 100,
        DELETION = 110,
        CONNECTION = 120,
        DISCONNECTION = 130,
        GETCONTACTLIST = 140,
        GETINVITATIONLIST = 150,
        GETCONTACT = 160,
        ADDCONTACT = 170,
        DELETECONTACT = 180,
        ACCEPTCONTACT = 190,
        DECLINECONTACT = 200,
        CHANGEPASSWORD = 210,
        STARTSESSION = 220,
        STOPSESSION = 230,
        ACCEPTSESSION = 240,
        DECLINESESSION = 250,
        ADDTOSESSION = 260,
        KICKFROMSESSION = 270
    };
}
#endif //BABIBEL_ACTIONTYPE_HPP
