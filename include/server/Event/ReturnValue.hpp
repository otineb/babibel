//
// Created by mymy on 28/09/17.
//

#ifndef BABIBEL_RETURNVALUE_HPP
#define BABIBEL_RETURNVALUE_HPP

/* prévenir els autres groupes que c 1 char lel */

enum class ReturnValue : unsigned char
{
  OK = 1,
  USERNAME_EXISTS = 2,
  WRONG_FORMAT_PWD = 3,
  WRONG_FORMAT_USER = 4,
  INVALID_USERNAME = 5,
  INVALID_PWD = 6,
  UNKNOWN_ERROR = 7
};

#endif //BABIBEL_RETURNVALUE_HPP
