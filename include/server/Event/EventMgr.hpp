//
// Created by mymy on 28/09/17.
//

#ifndef BABIBEL_EVENTMGR_HPP
#define BABIBEL_EVENTMGR_HPP

#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
#include "server/Event/ActionType.hpp"
#include "server/Network/Packet.hpp"
#include "server/Event/ReturnValue.hpp"

namespace babelServer {
    class EventMgr {
        std::unordered_map<ActionType, std::function<void(struct Event &, char *, unsigned int)>> _actionFunctions;
        std::unordered_map<ActionType, std::function<void(struct Event const&, Packet&)>> _responseFunctions;

    public:
        EventMgr();

        ~EventMgr() = default;

        Event   *translateInfo(babelServer::Packet);
        void  translateEvent(const Event *, Packet&);

    private:
        void actionWithTwoArgs(struct Event &, char *, unsigned int);
        void actionWithAnArg(struct Event &, char *, unsigned int);
        void actionWithNoneArg(struct Event &, char *, unsigned int);
    };
}

#endif //BABIBEL_EVENTMGR_HPP
