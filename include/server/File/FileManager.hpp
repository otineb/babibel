//
// Created by mymy on 27/09/17.
//

#ifndef BABIBEL_FILEMANAGER_HPP
#define BABIBEL_FILEMANAGER_HPP

#include <iostream>
#include <fstream>
#include <vector>

class FileManager
{

 private:
  std::string   _fileName;

 public:
  FileManager() {};
  FileManager(std::string const&);
  ~FileManager() = default;
  std::string           readFile() const;
  std::string           readFifo() const;
  void                  writeFile(std::string const&) const;
  void                  openFile(std::string const&);
  static std::string    readStdin();
  static void listFiles(std::string const&,
			std::vector<std::string>&);
};


#endif //BABIBEL_FILEMANAGER_HPP
