//
// Created by mymy on 30/09/17.
//

#ifndef BABIBEL_ASYSTEM_HPP
#define BABIBEL_ASYSTEM_HPP

namespace babelServer {

    class ChiefLeader;

    class ASystem {
    protected:
        ChiefLeader *_chief;

    public:
        ASystem(ChiefLeader *);

        ASystem();

        virtual ~ASystem() = default;

        virtual void getEvent(struct Event const *) = 0;

        void sendEvent(struct Event const&);

        void setChief(ChiefLeader *);
    };
}

#endif //BABIBEL_ASYSTEM_HPP
