//
// Created by mymy on 29/09/17.
//

#ifndef BABIBEL_PACKET_HPP
#define BABIBEL_PACKET_HPP

#include <iostream>
#include <boost/asio.hpp>
#include "server/Event/Event.hpp"

namespace babelServer {
    class Packet {
    public:
        unsigned int _size;
        const static unsigned int headerSize;
        const static unsigned int maxPacketSize = 512;
    private:
        char _data[maxPacketSize];
        boost::asio::ip::address _ipAddressUser;

    public:
        Packet();

        ~Packet() = default;


        int getSize() const;

        boost::asio::ip::address const &getIpAddressUser() const;

        char *getData();

        const char *getData() const;

        void setIp(boost::asio::ip::address const &);

        void clean();

        template <class T>
        struct  Protocol
        {
            int             size;
            unsigned short  code;
            T               data;
        private:
            Protocol();
        public:
            //template <>
            static void translateData(struct ResponseEvent const& e, Packet& p);
            static void translateData(struct Event const &e, Packet& p);
        };
    };
}
template <class T>
void babelServer::Packet::Protocol<T>::translateData(struct ResponseEvent const &e, Packet& p)
{
    p._size = sizeof(code) + sizeof(data);
    std::memcpy(p._data, &p._size, sizeof(p._size));
    std::memcpy(p._data + sizeof(p._size), &e.actionType, sizeof(e.actionType));
    std::memcpy(p._data + sizeof(e.actionType) + sizeof(p._size), &e.ret, sizeof(e.ret)); //T == T(e.ret)
}

template <class T>
void babelServer::Packet::Protocol<T>::translateData(struct Event const &e, Packet& p)
{
    p._size = 0;
    //code action
    std::memcpy(p._data + sizeof(p._size), &e.actionType, sizeof(e.actionType));
    for (auto const& s : e.data)
    {
        std::memcpy(p._data + sizeof(p._size) + sizeof(e.actionType) + p._size, s.data(), s.length() + 1);
        p._size += s.length();
        p._size++;
    }
    p._size += sizeof(e.actionType);
    std::memcpy(p._data, &p._size, sizeof(p._size));
}

#endif //BABIBEL_PACKET_HPP
