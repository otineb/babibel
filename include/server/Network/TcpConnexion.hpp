//
// Created by benito on 27/09/17.
//

#ifndef BABIBEL_TCPCONNEXION_HPP
#define BABIBEL_TCPCONNEXION_HPP

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <mutex>
#include <queue>
#include "server/Network/Packet.hpp"
#include "server/ASystem.hpp"

namespace babelServer {
    class TcpConnexion : public std::enable_shared_from_this<TcpConnexion>, public ASystem {
    private:
        TcpConnexion(boost::asio::io_service &io_service, ChiefLeader *);

        boost::asio::ip::tcp::socket socket_;
        std::string message_;
        Packet packet_;
        Packet writtenPacket_;
      	std::mutex mutie_;
        std::queue<Packet> packetQueue_;

    public:
        static std::shared_ptr<TcpConnexion> create(boost::asio::io_service &io_service, ChiefLeader *);

        boost::asio::ip::tcp::socket &socket();

        virtual void getEvent(struct Event const *) override;

        void start();

        void writeMessage(Packet const& p);

        boost::asio::ip::address const & getIp() const;

        ~TcpConnexion();

    private:
        //void handleWriteHeader(const boost::system::error_code & /*error*/,
         //                size_t /*bytes_transferred*/);
        void handleWrite(const boost::system::error_code & /*error*/,
                               size_t /*bytes_transferred*/);

        void handleReadHeader(const boost::system::error_code & /*error*/);

        void handleReadBody(const boost::system::error_code &);

        void disconnect();

        void shutdown();
    };
}

#endif //BABIBEL_TCPCONNEXION_HPP
