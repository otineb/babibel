//
// Created by benito on 27/09/17.
//

#ifndef BABIBEL_NETWORKMANAGER_HPP
#define BABIBEL_NETWORKMANAGER_HPP

#include "server/Network/TcpConnexion.hpp"
#include "server/ASystem.hpp"

namespace babelServer {

    class ChiefLeader;

    class NetworkManager : public ASystem {
    private:
        boost::asio::ip::tcp::acceptor acceptor_;
        std::vector<std::shared_ptr<TcpConnexion>> clients_;

    public:
        NetworkManager(boost::asio::io_service &io_service);

        void start_accept();

        virtual void getEvent(struct Event const *) override;
        static std::vector<ActionType> handledActions;

    private:
        void destroy(struct Event const *e);
        void handle_accept(std::shared_ptr<TcpConnexion> const& new_connection,
                           const boost::system::error_code &error);

    private:
        void writePacket(Packet const&);
    };
}
#endif //BABIBEL_NETWORKMANAGER_HPP
